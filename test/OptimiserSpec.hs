{-# LANGUAGE OverloadedStrings #-}
module OptimiserSpec (spec) where

import Control.Monad.State (execState, modify)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Test.Hspec

import AudioGraphOptimisations (removeUnconnectedNodes)
import Cape.Types.Session (NodeName(..))
import Cape.Types.Graph
  ( newGraph, addNode, connect, addInput, nodeBody
  , Node, addNode, Node(..), InPortId(..) , OutPortId(..))

instance (Show b) => Show (Node b d) where
    show = show . nodeBody

spec :: Spec
spec = do
    describe "removeUnconnectedNodes" $ do
        it "does nothing to minimal" $
          let
            nid = NodeName "tom"
            g = execState oneConnectedNode $ fromJust $ addNode nid b emptyGraph
            oneConnectedNode = do
                modify $ addInput "intoWorld"
                modify $ connect (OutPortId (Just nid, 0)) (InPortId (Nothing, 0))
          in
            removeUnconnectedNodes g `shouldBe` (g, Map.empty)
        it "returns stripped" $
            strippedNodes
            `shouldBe`
            (Map.singleton nodeId $ Node b ["mcnodeface"] ["nodey"])
        it "strips unconnected" $ optimisedGraph `shouldBe` emptyGraph
  where
    emptyGraph = newGraph (\(s, _) -> [s]) (\(_, s) -> [s])
    b = ("nodey", "mcnodeface" :: [Char])
    nodeId = NodeName "panda"
    graph = fromJust $ addNode nodeId b emptyGraph
    (optimisedGraph, strippedNodes) = removeUnconnectedNodes graph
