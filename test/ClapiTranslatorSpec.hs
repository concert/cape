{-# LANGUAGE
    OverloadedStrings
  , QuasiQuotes
  , StandaloneDeriving
#-}

module ClapiTranslatorSpec (spec) where

import Control.Monad.State (evalState)
import qualified Data.Map.Mol as Mol
import Test.Hspec

import Clapi.TH (pathq)
import Clapi.Types.Path (Path)
import Rage.Graph (TransportState(TransportRolling))

import ClapiTranslator
  ( eventToClapi, ApiControlEvent(..), ApiUpdateEvent(..), root, engineN
  , GraphUpdateEvent(..), GraphControlEvent(..), PreTrpd(..))
import Clapi.Types
  ( Time(..), someWv, WireType(..)
  , DataChange(ConstChange), DataErrorIndex(PathError)
  , FrDigest(..), someWireable)
import qualified Clapi.Types.AssocList as AL
import Router (handleFrpDigest)

deriving instance Eq PreTrpd
deriving instance Show PreTrpd

-- ClapiTranslator

transportStateChangedDcp :: (Path, DataChange)
transportStateChangedDcp = (
    [pathq|/transport/state|], ConstChange Nothing [someWv WtWord32 1])

spec :: Spec
spec = do
    it "ddToEvents" $
        (Mol.singleton (PathError [pathq|/sommat|]) "/: Missing child router", [AEGraph $ GCTranspSet TransportRolling])
        `shouldBe`
        (ddToEvents $ AL.fromList [transportStateChangedDcp, badDcp])
    it "eventsToDcps" $
        (mempty {ptrpdData = AL.fromList [transportStateChangedDcp, transportChangedDcp]})
        `shouldBe`
        (foldl mappend mempty $ map
            (\es -> fst $ evalState (eventToClapi es) mempty)
            [AUGraph $ GUTranspSet TransportRolling $ Time 12 30])
  where
    badDcp = ([pathq|/sommat|], ConstChange Nothing [someWv WtWord32 3])
    ddToEvents dd = handleFrpDigest root $ Frpd engineN dd mempty mempty
    transportChangedDcp = ([pathq|/transport/changed|], ConstChange Nothing [someWireable $ Time 12 30])
