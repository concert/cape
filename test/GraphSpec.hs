{-# LANGUAGE OverloadedStrings #-}
module GraphSpec (spec) where

import Test.Hspec

import Cape.Types.Graph (adjustNode, newGraph, addNode, Graph)
import Cape.Types.Session (NodeName(..))

spec :: Spec
spec = do
    it "adjusts" $ ga `shouldBe` gb
  where
    inid = NodeName "initial"
    gnid = NodeName "interest"
    sngl i = [i]
    startGraph = case addNode inid 7 $ newGraph sngl sngl of
        Nothing -> error "Failed to add initial node"
        Just g -> g
    ga, gb :: Maybe (Graph NodeName Int Int)
    ga = adjustNode (+1) gnid <$> addNode gnid (3 :: Int) startGraph
    gb = addNode gnid 4 startGraph
