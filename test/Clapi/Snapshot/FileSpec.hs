module Clapi.Snapshot.FileSpec (spec) where

import Data.Functor.Identity (Identity(..))
import qualified Data.Text.Lazy as Text
import Test.Hspec
import Text.Trifecta

import Clapi.Types.Tree (TreeType(TtInt32), unbounded)
import Clapi.Types.Base (Interpolation(..), Time(..))
import Clapi.Snapshot.File

spec :: Spec
spec = do
    describe "roundtrips" $ do
        it "single value" $ roundTripLeaf $ LsSingle Nothing [TypedValues (TtInt32 unbounded) $ Identity 12]
        it "full series" $ roundTripLeaf $  LsSeries
          [ (Nothing, Time 0 0, IConstant)
          , (Nothing, Time 1 0, IConstant)
          , (Nothing, Time 2 0, IConstant)
          ]
          [TypedValues (TtInt32 unbounded) [1, 2, 3]]

roundTripLeaf :: LeafSnapshot -> IO ()
roundTripLeaf ls =
  let
    lt = Text.unpack $ contentsText ls
  in
    case parseString (contentsP mempty) mempty lt of
        Success rvs -> rvs `shouldBe` ls
        Failure ei -> error $ "Serialised as:\n" ++ lt ++ "\nParse failed:" ++ show ei
