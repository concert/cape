{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}
import Data.List (isInfixOf)
import qualified Data.Map as Map
import System.Environment (getEnv)
import Control.Monad.Except (runExceptT)
import Control.Monad.Fail (MonadFail)
import Control.Concurrent (threadDelay)
import Control.Concurrent.MVar
import Data.Maybe (fromJust)

import Clapi.Types.Base (Time(..))
import Clapi.TH (n)
import qualified Clapi.Types.AssocList as AL
import qualified Clapi.Types.Dkmap as Dkm

import qualified Rage.Jack as J
import qualified Rage.Loader as L
import Rage.Graph (TransportState(..))
import Rage.Types.Atoms (Atom(..))
import Rage.Types.Backend (SampleRate(..), PeriodSize(..))

import Rendering (mkLive, GetPersistType, PersistType(..), PersistMode(..))
import Cape.Types.AudioSource (SomeAudioSource(..), AudioSource(..), takeSlice)
import Cape.Types.TimeOrigin (TimeOrigin(..))
import Cape.Types.Element (Element(..))
import Cape.Types.Graph (addOutput)
import Cape.Types.Session (zt, newAudioGraph, NodeName(..))
import RageGraphSync
  ( withRunningGraph, applyTransp, applyGraph
  , RageTransportControl(RtcTranspSetState), RageGraphControl(..), REError
  , RageSessionEvent, RunningGraph)

persistGetter :: IO (GetPersistType)
persistGetter = do
    l <- getEnv "RAGE_ELEMENTS_PATH" >>= L.loader
    kindList <- L.getKindList l
    let persistKind = head $ filter (isInfixOf "persist") kindList
    mPK <- L.loadKind persistKind
    pK <- mPK
    pCache <- newMVar mempty
    pure $ \mode nChans -> do
        existingPTys <- takeMVar pCache
        case Map.lookup (mode, nChans) existingPTys of
            Nothing -> do
                mTy <- L.mkType pK [[AInt nChans], [AEnum $ fromEnum mode]]
                case mTy of
                    Left msg -> error $ "Persist type build failed: " <> msg
                    Right ty -> let pTy = PersistType ty in do
                        putMVar pCache $ Map.insert (mode, nChans) pTy existingPTys
                        pure pTy
            Just pTy -> do
                putMVar pCache existingPTys
                pure pTy

baseSrc :: SomeAudioSource
baseSrc = SomeAudioSource $ SFile (TimeOrigin 0) (Time 30 0) 2 "/home/dave/music/L/goomh_string_example.wav"

topSrc :: SomeAudioSource
topSrc = SomeAudioSource $ VFile
    (TimeOrigin 1)
    (Time 20 0)
    (AL.fromMap $ Map.fromList
      [ ([n|a|]
      , fromJust $ Dkm.insert 0 zt (Nothing, fromJust $ takeSlice baseSrc zt (Time 15 0)) Dkm.empty)])
    (addOutput L.StreamAudio $ addOutput L.StreamAudio newAudioGraph)
    "/tmp/minifile.wav"

main :: IO ()
main = do
    getPTy <- persistGetter
    liveElem <- mkLive getPTy topSrc
    wjbe <- J.withJackBackend (SampleRate 44100) (PeriodSize 1024) ["in0", "in1"] ["out0", "out1"] $ \be -> do
        r <- runExceptT $ withRunningGraph be $ needsTypeAnnotation liveElem
        either error pure r
    either error id wjbe
    -- Required to keep the type cache in scope:
    getPTy Playback 2 >> return ()
  where
    eNid = NodeName "phil"
    needsTypeAnnotation :: MonadFail f => Element -> IO (Maybe String, [Either REError RageSessionEvent]) -> RunningGraph (Maybe String) x -> IO (f ())
    needsTypeAnnotation liveElem nextEvt rg = do
        applyGraph rg (RgcElementAdd eNid (elementType liveElem) (timeSeries liveElem))
        applyGraph rg (RgcConnect (Just eNid, 0) (Nothing, 0))
        applyGraph rg (RgcConnect (Just eNid, 1) (Nothing, 1))
        applyTransp rg (RtcTranspSetState TransportRolling)
        threadDelay 10000000
        applyTransp rg (RtcTranspSetState TransportStopped)
        pure $ pure ()
