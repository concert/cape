{-# LANGUAGE
    OverloadedStrings
  , QuasiQuotes
  , TypeApplications
  , TupleSections
#-}
module Main where

import Control.Monad.Fail (MonadFail)
import Data.Map (Map)
import qualified Data.Map as Map
import System.Environment (getEnv)
import Control.Monad.Except (runExceptT)
import qualified Data.Text as Text

import Clapi.Types.Name (DataName, mkName)

import Cape.Types.Session (Session(..), transportInfoEmpty, newAudioGraph)
import Rage.Loader (getKindList, StreamDef(..))
import qualified Rage.Jack as J
import qualified Rage.Loader as L
import Rage.Types.Backend (SampleRate(..), PeriodSize(..))
import RageGraphSync (withRunningGraph)
import Cape.Types.Graph (addInput, addOutput)
import ClapiEventApplier (LoaderResults(..))
import Server (serve)

-- The string munging here should go once we have metadata loading in RAGE
asLoadable :: MonadFail m => [String] -> m (Map DataName String)
asLoadable = fmap Map.fromList . mapM (\t -> (, t) <$> mkName (
    fst $ Text.breakOn "." $ Text.drop 3 $ snd $ Text.breakOnEnd "/" $ Text.pack t))

main :: IO ()
main = do
    p <- getEnv "RAGE_ELEMENTS_PATH"
    loader <- L.loader p
    elem_names <- getKindList loader
    loadables <- asLoadable elem_names
    mf <- wjbe $ \be -> do
        r <- runExceptT $ withRunningGraph be $ mainHandler
            (LoaderResults loadables mempty mempty mempty)
            $ Session graph transportInfoEmpty
        case r of
            Left msg -> error msg
            Right () -> pure ()
    mf
  where
    mainHandler loaderResults aps beNext be = serve (beNext, be) loaderResults aps
    graph =
        addOutput StreamAudio $ addOutput StreamAudio $
        addInput StreamAudio $ addInput StreamAudio $
        newAudioGraph
    wjbe = J.withJackBackend
        (SampleRate 44100) (PeriodSize 1024) ["in0", "in1"] ["out0", "out1"]
