module AudioGraphOptimisations where
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import Cape.Types.Graph
  ( Graph(graphNodes, graphConnections), Node, InPortId(..) , OutPortId(..))
import Clapi.Protocol (Protocol, waitThen, sendFwd, sendRev)
import Cape.Types.Session (Session(..), NodeName)

removeUnconnectedNodes :: Graph NodeName b d -> (Graph NodeName b d, Map NodeName (Node b d))
removeUnconnectedNodes g = (g{graphNodes = connectedNodes}, unconnectedNodes)
  where
    connectedNodes = Map.restrictKeys (graphNodes g) connectedNodeSet
    unconnectedNodes = Map.withoutKeys (graphNodes g) connectedNodeSet
    connectsNodes acc (OutPortId (na, _), InPortId (nb, _)) = addJust na $ addJust nb acc
      where
        addJust mn a = case mn of
            Just nid -> Set.insert nid a
            Nothing -> a
    connectedNodeSet = Set.foldl connectsNodes Set.empty $ graphConnections g

restoreUnconnectedNodes :: Map NodeName (Node b d) -> Graph NodeName b d -> Graph NodeName b d
restoreUnconnectedNodes ns g = g{graphNodes = Map.union (graphNodes g) ns}

stripUnconnectedProto :: (Monad m) => Protocol Session Session Session Session m ()
stripUnconnectedProto = sup Map.empty
  where
    sup ns = waitThen fwd (rev ns)
    fwd ps = let (ng, ns) = removeUnconnectedNodes $ apsGraph ps in sendFwd ps{apsGraph = ng} >> sup ns
    rev ns ps = sendRev ps{apsGraph = restoreUnconnectedNodes ns $ apsGraph ps} >> sup ns
