{-# LANGUAGE LambdaCase, OverloadedStrings, GADTs #-}
module Clapi.Snapshot.TreeTypes (ttToText, ttP) where

import Control.Applicative ((<|>))
import Data.Scientific (toRealFloat)
import qualified Data.Text as Text
import Data.Text (Text)
import Text.Trifecta

import Clapi.Snapshot.Paths (nameP)
import Clapi.Snapshot.Util (mfp)
import Clapi.Types ()  -- MonadFail (Either String)
import Clapi.Types.Base (typeEnumOf)
import Clapi.Types.Name (unName)
import qualified Clapi.Types.SymbolList as SL
import Clapi.Types.Tree
  ( SomeTreeType(..), TreeType(..), TreeTypeName(..)
  , Bounds, bounds, unbounded, boundsMin, boundsMax
  , ttTime, ttEnum, ttWord32, ttWord64, ttInt32, ttInt64, ttFloat, ttDouble
  , ttString, ttRef, ttList, ttSet, ttOrdSet, ttMaybe, ttPair)

ttP :: (Monad m, TokenParsing m) => m SomeTreeType
ttP = ttNameP >>= argsParser
  where
    argsParser = \case
        TtnTime -> pure ttTime
        TtnEnum -> ttEnum <$> fmap (Text.unpack . unName)
            <$> optionalBracket [] (commaSep nameP)
        TtnWord32 -> ttWord32 <$> bbp (fromIntegral <$> natural)
        TtnWord64 -> ttWord64 <$> bbp (fromIntegral <$> natural)
        TtnInt32 -> ttInt32 <$> bbp (fromIntegral <$> integer)
        TtnInt64 -> ttInt64 <$> bbp (fromIntegral <$> integer)
        TtnFloat -> ttFloat <$> bbp (toRealFloat <$> scientific)
        TtnDouble -> ttDouble <$> bbp (toRealFloat <$> scientific)
        TtnString -> ttString <$> optionalBracket "" stringLiteral
        TtnRef -> ttRef <$> brackets nameP
        TtnList -> brackets $ ttList <$> ttP
        TtnSet -> brackets $ ttSet <$> ttP
        TtnOrdSet -> brackets $ ttOrdSet <$> ttP
        TtnMaybe -> brackets $ ttMaybe <$> ttP
        TtnPair -> brackets $ ttPair <$> ttP <*> ttP
    bbp p = optionalBracket unbounded $ do
        mMinB <- optional p
        _ <- colon
        mMaxB <- optional p
        mfp $ bounds mMinB mMaxB
    optionalBracket dv p = brackets p <|> pure dv

ttNameP :: (TokenParsing m, Monad m) => m TreeTypeName
ttNameP = choice $
    (\ctn -> textSymbol (ttnToText ctn) >> return ctn) <$> [minBound..]

ttnToText :: TreeTypeName -> Text
ttnToText = \case
    TtnTime -> "time"
    TtnEnum -> "enum"
    TtnWord32 -> "word32"
    TtnWord64 -> "word64"
    TtnInt32 -> "int32"
    TtnInt64 -> "int64"
    TtnFloat -> "float"
    TtnDouble -> "double"
    TtnString -> "str"
    TtnRef -> "ref"
    TtnList -> "list"
    TtnSet -> "set"
    TtnOrdSet -> "ordset"
    TtnMaybe -> "maybe"
    TtnPair -> "pair"

ttToText :: TreeType a -> Text
ttToText tt = (ttnToText $ typeEnumOf tt) <> bracketContent tt
  where
    ib s = "[" <> s <> "]"
    bracketContent :: TreeType a -> Text
    bracketContent = \case
        TtTime -> ""
        TtEnum sl -> ib $ Text.intercalate "," $ Text.pack <$> SL.toStrings sl
        TtWord32 b -> bb b
        TtWord64 b -> bb b
        TtInt32 b -> bb b
        TtInt64 b -> bb b
        TtFloat b -> bb b
        TtDouble b -> bb b
        TtString r -> ib $ "\"" <> r <> "\""
        TtRef s -> ib $ unName s
        TtList tt' -> ib $ ttToText tt'
        TtSet tt' -> ib $ ttToText tt'
        TtOrdSet tt' -> ib $ ttToText tt'
        TtMaybe tt' -> ib $ ttToText tt'
        TtPair tt0 tt1 -> ib $ ttToText tt0 <> "," <> ttToText tt1
    bb :: Show a => Bounds a -> Text
    bb b = case (boundsMin b, boundsMax b) of
        (Nothing, Nothing) -> ""
        (l, u) -> ib $ om l <> ":" <> om u
    om :: Show a => Maybe a -> Text
    om = maybe "" (Text.pack . show)
