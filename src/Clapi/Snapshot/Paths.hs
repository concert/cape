module Clapi.Snapshot.Paths where

import Control.Applicative ((<|>))
import qualified Data.Text as Text
import Text.Trifecta

import Clapi.Types ()  -- MonadFail (Either String)
import Clapi.Types.Path (Path'(..), Path)
import Clapi.Types.Name (Name, mkName)
import Clapi.Snapshot.Util (mfp)

nameP :: (TokenParsing m, Monad m) => m (Name a)
nameP = some (alphaNum <|> char '_') >>= mfp . mkName . Text.pack

pathP :: (TokenParsing m, Monad m) => m Path
pathP = fmap Path' $ char '/' >> sepBy1 nameP (char '/')
