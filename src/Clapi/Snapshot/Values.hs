{-# LANGUAGE
    LambdaCase, GADTs, ScopedTypeVariables, TypeOperators, FlexibleContexts
  , Rank2Types, TupleSections, OverloadedStrings #-}
module Clapi.Snapshot.Values (getValP, valToText, maybeP, maybeText, timeP, timeText) where

import Control.Applicative ((<|>))
import Data.Constraint (Dict(..))
import Data.Scientific (toRealFloat)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Text.Regex.PCRE ((=~~))
import Text.Trifecta

import Clapi.Snapshot.Paths (pathP)
import Clapi.Snapshot.Util (mfp)
import Clapi.Types ()  -- MonadFail (Either String)
import Clapi.Types.Base (Time(..))
import Clapi.Types.EnumVal (EnumVal(..), enumName)
import Clapi.Types.Nat (SNat(..), type (<), (:<:)(..), ltDict)
import Clapi.Types.Path (Path)
import Clapi.Types.Name (DefName, unName)
import qualified Clapi.Types.Path as Path
import qualified Clapi.Types.Symbol as Sym
import Clapi.Types.SymbolList (SymbolList(..), Length)
import qualified Clapi.Types.SymbolList as SL
import Clapi.Types.Tree (TreeType(..), getTtOrd, getTtShow)
import Clapi.Types.UniqList (mkUniqList, unUniqList)
import Clapi.Validator (inBounds)

maybeP :: (Monad m, TokenParsing m) => m a -> m (Maybe a)
maybeP sub = (symbol "%Nothing" >> pure Nothing) <|> (Just <$> sub)

getValP
  :: forall a m. (Show a, Ord a, TokenParsing m, Monad m) => (Path -> Maybe DefName) -> TreeType a -> m a
getValP getType = \case
    TtTime -> timeP
    TtEnum sl -> enumOptsP sl
    TtWord32 b -> natural >>= mfp . inBounds b . fromIntegral
    TtWord64 b -> natural >>= mfp . inBounds b . fromIntegral
    TtInt32 b -> integer >>= mfp . inBounds b . fromIntegral
    TtInt64 b -> integer >>= mfp . inBounds b . fromIntegral
    TtFloat b -> scientific >>= mfp . inBounds b . toRealFloat
    TtDouble b -> scientific >>= mfp . inBounds b . toRealFloat
    TtString r -> literalP r
    TtRef t -> pathP >>= \rp -> if getType rp == Just t
        then pure rp
        else fail "Ref to path of incorrect type"
    TtList tt -> sub tt $ brackets . commaSep
    TtSet tt ->  sub tt $ fmap Set.fromList . braces . commaSep
    TtOrdSet tt -> sub tt $ \p -> angles (commaSep p) >>= mfp . mkUniqList
    TtMaybe tt -> sub tt maybeP
    TtPair tt0 tt1 -> sub tt0 $ \p0 -> parens $ do
        v0 <- p0
        _ <- comma
        sub tt1 (\p1 -> (v0,) <$> p1)
  where
    sub :: TreeType b -> ((Show b, Ord b) => m b -> m a) -> m a
    sub tt f = case (getTtOrd tt, getTtShow tt) of
        (Dict, Dict) -> f $ getValP getType tt

timeP :: (TokenParsing m, Monad m) => m Time
timeP = do
  s <- fromIntegral <$> natural
  _ <- char ':'
  sf <- fromIntegral <$> natural
  return $ Time s sf

enumOptsP :: TokenParsing m => SymbolList a -> m (EnumVal a)
enumOptsP sl = choice $
    (enumerate (\n s -> const (EnumVal sl n) <$> symbol (Sym.toString s))) sl

literalP :: (TokenParsing m, Monad m) => Text.Text -> m Text.Text
literalP r = do
    v <- stringLiteral
    case Text.unpack v =~~ Text.unpack r of
        Just () -> pure v
        Nothing -> fail $ "Did not match regex: " ++ Text.unpack r

ts :: Show a => a -> Text
ts = Text.pack . show

maybeText :: (a -> Text) -> Maybe a -> Text
maybeText atxt = maybe "%Nothing" atxt

timeText :: Time -> Text
timeText (Time s f) = ts s <> ":" <> ts f

valToText :: TreeType a -> a -> Text
valToText = \case
    TtTime -> timeText
    TtEnum _ -> Text.pack . enumName
    TtWord32 _ -> ts
    TtWord64 _ -> ts
    TtInt32 _ -> ts
    TtInt64 _ -> ts
    TtFloat _ -> ts
    TtDouble _ -> ts
    TtString _ -> \t -> "\"" <> Text.replace "\"" "\\\"" t <> "\""
    TtRef _ -> Path.toText unName
    TtList tt -> csb "[" "]" tt
    TtSet tt -> csb "{" "}" tt . Set.toList
    TtOrdSet tt -> csb "<" ">" tt . unUniqList
    TtMaybe tt -> maybeText $ valToText tt
    TtPair tt0 tt1 -> \(v0, v1) ->
        "(" <> valToText tt0 v0 <> "," <> valToText tt1 v1 <> ")"
  where
    csb l r tt vs = l <> Text.intercalate "," (valToText tt <$> vs) <> r

-- FIXME: Probably doesn't belong here:
enumerate
  :: forall a r. (forall n s. n < Length a => SNat n -> Sym.SSymbol s -> r)
  -> SymbolList a -> [r]
enumerate f sl = go (ltProof SZero $ SL.length sl) sl SZero
  where
    ltProof :: SNat m -> SNat n -> Maybe (m :<: n)
    ltProof SZero (SSucc _) = Just LT1
    ltProof (SSucc m) (SSucc n) = LT2 <$> ltProof m n
    ltProof _ _ = Nothing
    go :: Maybe (n :<: Length a) -> SymbolList b -> SNat n -> [r]
    go (Just p) (SlCons s sl') n = case ltDict p of
        Dict -> f n s : go (ltProof (SSucc n) (SL.length sl)) sl' (SSucc n)
    go _ _ _ = []
