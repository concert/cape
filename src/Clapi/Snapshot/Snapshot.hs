module Clapi.Snapshot.Snapshot where

import qualified Data.Map as Map
import Data.Map (Map)
import qualified Data.Text.Lazy.IO as TIO
import qualified Data.Text as T
import System.FilePath (takeDirectory, dropTrailingPathSeparator, (</>))
import System.Directory (createDirectoryIfMissing, listDirectory, doesDirectoryExist)
import Text.Trifecta (parseFromFile)

import Clapi.Snapshot.File
import Clapi.Types.Path (Path, fromText)
import Clapi.Types.Name (nameP)

type Snapshot = Map Path LeafSnapshot

writeSnapshot :: Snapshot -> FilePath -> IO ()
writeSnapshot s fp = mapM_ writePath $ Map.toList s
  where
    writePath (p, tv) = let dp = clapiPathToFilePath fp p in do
        createDirectoryIfMissing True $ takeDirectory dp
        TIO.writeFile dp $ contentsText tv

clapiPathToFilePath :: FilePath -> Path -> FilePath
clapiPathToFilePath bp p = bp ++ show p

readSnapshot :: FilePath -> IO Snapshot
readSnapshot fp = Map.fromList <$> (getFilePaths fp >>= mapM readPath)
  where
    -- FIXME: Will not play nicely with refs:
    p = contentsP $ const Nothing
    readPath rfp = do
        m <- parseFromFile p rfp
        case m of
            Nothing -> error $ "Could not parse " ++ rfp
            Just tvs -> pure (filePathToClapiPath fp rfp, tvs)

filePathToClapiPath :: FilePath -> FilePath -> Path
filePathToClapiPath baseFp fp =
  let
    trimmedFp = drop (length $ dropTrailingPathSeparator baseFp) fp
  in
     case fromText nameP $ T.pack trimmedFp of
        Nothing -> error $ "Illegal Clapi Path " ++ trimmedFp
        Just cp -> cp

getFilePaths :: FilePath -> IO [FilePath]
getFilePaths fp = fmap concat $ listDirectory fp >>= mapM go
  where
    go seg = let sfp = fp </> seg in do
        isDir <- doesDirectoryExist sfp
        if isDir
          then getFilePaths sfp
          else pure [sfp]
