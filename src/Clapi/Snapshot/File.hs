{-# LANGUAGE GADTs, OverloadedStrings, StandaloneDeriving, RankNTypes, LambdaCase #-}
module Clapi.Snapshot.File where

import Control.Applicative ((<|>))
import Data.Constraint (Dict(..))
import Data.Functor.Identity (Identity(..))
import Data.Type.Equality ((:~:)(Refl), testEquality)
import Data.List as List
import qualified Data.Text.Lazy as Text
import Data.Text.Lazy (Text)
import Text.Trifecta

import Clapi.Snapshot.TreeTypes
import Clapi.Snapshot.Values
import Clapi.Types.Path (Path)
import Clapi.Types.Name (DefName)
import Clapi.Types.Tree (TreeType, withTreeType, getTtOrd, getTtShow)
import Clapi.Types.Base (Attributee(..), Time, Interpolation(..))

data TypedValues f where
    TypedValues :: TreeType a -> f a -> TypedValues f

class SEKind f where
    sefShow :: (forall a. Show a => f a -> String)
    sefEq :: (forall a. Eq a => f a -> f a -> Bool)

instance SEKind f => Show (TypedValues f) where
    show (TypedValues tt vs) = case getTtShow tt of
        Dict -> "TypedValues " ++ show tt ++ " " ++ sefShow vs

instance SEKind f => Eq (TypedValues f) where
    (TypedValues tt0 vs0) == (TypedValues tt1 vs1) = case testEquality tt0 tt1 of
        Nothing -> False
        Just Refl -> case getTtOrd tt0 of
            Dict -> sefEq vs0 vs1

instance SEKind [] where
    sefShow = show
    sefEq = (==)

type SeriesValue = TypedValues []

instance SEKind Identity where
    sefShow = show
    sefEq = (==)

type SingleValue = TypedValues Identity

data LeafSnapshot
  = LsSingle (Maybe Attributee) [SingleValue]
  -- FIXME: These lists aren't the same, first one is one per row, second is
  -- per column containing lists that are one per row:
  | LsSeries [(Maybe Attributee, Time, Interpolation)] [SeriesValue]
  deriving (Eq, Show)

singleContentsText :: [SingleValue] -> Text
singleContentsText = Text.concat . fmap tvt
  where
    tvt (TypedValues t (Identity v)) =
        Text.fromStrict (ttToText t) <> " " <>
        Text.fromStrict (valToText t v) <> "\n"

singleContentsP :: (Path -> Maybe DefName) -> Parser [SingleValue]
singleContentsP getType = many (runUnlined parseRow) <* eof
  where
    parseRow = ttP >>= (withTreeType $ \tt -> case (getTtOrd tt, getTtShow tt) of
        (Dict, Dict) -> do
            v <- getValP getType tt
            _ <- some newline
            pure $ TypedValues tt $ Identity v)

seriesText :: SeriesValue -> [Text]
seriesText (TypedValues t vs) =
    Text.fromStrict (ttToText t) : fmap (Text.fromStrict . valToText t) vs

seriesHeadingP :: (Monad m, TokenParsing m) => m SeriesValue
seriesHeadingP = withTreeType (\tt -> TypedValues tt []) <$> ttP

contentsText :: LeafSnapshot -> Text
contentsText = \case
    LsSingle a v -> "const\n" <> "attributee " <> atxt a <> "\n" <> singleContentsText v
    LsSeries ati v ->
      let
        headings : rows = transpose $ seriesText <$> v
        body = zipWith assemble ati rows
        assemble (a, t, i) row =
            atxt a <> " " <>
            Text.fromStrict (timeText t) <> " " <>
            Text.intercalate " " row <> " " <>
            Text.pack (show i)
      in "series\nattributee time " <> Text.intercalate " " headings <> " interpolation\n" <> Text.intercalate "\n" body <> "\n"
  where
    atxt = Text.fromStrict . maybeText unAttributee

contentsP :: (Path -> Maybe DefName) -> Parser LeafSnapshot
contentsP getType =
    (text "const\n" >> single) <|> (text "series\n" >> series)
  where
    single = LsSingle
        <$> (textSymbol "attributee" >> attrP)
        <*> singleContentsP getType
    series = runUnlined $ do
        heads <- getHeads
        (ati, vals) <- getVals mempty heads
        pure $ LsSeries ati vals
    getHeads = do
        _ <- textSymbol "attributee" >> textSymbol "time"
        tyHeads <- some seriesHeadingP
        _ <- textSymbol "interpolation"
        pure tyHeads
    getVals ati vals = do
        _ <- some newline
        ended <- optional eof
        case ended of
            Just _ -> pure (reverse ati, reverseTvs <$> vals)
            Nothing -> do
                a <- attrP
                t <- timeP
                vals' <- mapM consVal vals
                i <- interP
                getVals ((a, t, i) : ati) vals'
    consVal (TypedValues tt vs) = case (getTtOrd tt, getTtShow tt) of
        (Dict, Dict) -> do
            v <- getValP getType tt
            pure $ TypedValues tt $ v : vs
    reverseTvs (TypedValues tt vs) = TypedValues tt $ reverse vs
    attrP :: (TokenParsing m, Monad m) => m (Maybe Attributee)
    attrP = fmap Attributee <$> maybeP stringLiteral
    interP = choice
      [ textSymbol "IConstant" >> pure IConstant
      , textSymbol "ILinear" >> pure ILinear
      ]
