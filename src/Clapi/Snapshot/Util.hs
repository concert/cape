module Clapi.Snapshot.Util where
import Text.Trifecta

mfp :: Parsing p => Either String a -> p a
mfp = either unexpected pure
