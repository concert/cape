{-# LANGUAGE
   DataKinds
 , OverloadedStrings
 , PatternSynonyms
 , QuasiQuotes
 , TypeApplications
 , TupleSections
 , GADTs
 , LambdaCase
#-}

module ClapiTranslator where

import Prelude hiding (fail)
import Control.Monad.Fail (MonadFail(..))
import Control.Monad.State (State, get, put, runState)
import Data.Bifunctor (first)
import Data.Functor.Identity (Identity(..))
import Data.Proxy
import Data.Word
import Data.Text (Text)
import qualified Data.Text as T
import Control.Monad (unless)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.List as List
import Text.Regex.PCRE ((=~~))
import Data.Maybe (fromJust)
import Data.Type.Equality ((:~:)(..))

import Data.Map.Mol (Mol)
import qualified Data.Map.Mol as Mol

import Clapi.Snapshot.Snapshot (Snapshot)
import Clapi.Snapshot.File (TypedValues(..), SeriesValue, LeafSnapshot(..))
import Clapi.Types
  ( Time(..), Interpolation, InterpolationLimit(..), Editability(..)
  , ContOps, Attributee(..)
  , CreateOp(..), Placeholder(..)
  , FrDigest(..), TrpDigest, trpdEmpty
  , DataChange(..), DataDigest, DataErrorIndex(..)
  , structDef, arrayDef, tupleDef, DefOp(..), PostDefinition(..)
  , DefName, SomeDefinition(..)
  , WireType(..), SomeWireType, castWireValue, SomeWireValue(..), someWv, WireValue(..), someWireable
  , PostDefinition, structDef
  , TpId, TimeSeriesDataOp
  , ttRef, ttEnum', ttTime
  , TrDigest(..), SomeFrDigest(..)
  )
import Clapi.Types.AssocList (AssocList)
import qualified Clapi.Types.AssocList as AL
import Clapi.Types.Definitions (getTyInfoForName, Definition(TupleDef), tupDefTys)
import Clapi.Types.Path
  (Path, pattern Root, pattern (:/), pattern (:</), toText)
import Clapi.Types.Name
  ( Name, NameRole(..), PostDefName, DataName, Namespace
  , mkName, unName, castName)
import Clapi.Types.SequenceOps (SequenceOp(..))
import Clapi.Types.Tree (SomeTreeType(..), TreeType(TtMaybe, TtString), ttString)
import Clapi.Tree (TimeSeries)
import Clapi.TH (n)
import Clapi.Relay (oppifyTimeSeries)
import Clapi.Validator (typeValid, inflateValue)

import Router

import Rage.Graph (TransportState(TransportStopped))
import Rage.Loader (TupleDef)
import DefConv (rtdsToCro, croToPd, croToStructDef, ClapiReadyOpts)

-- FIXME: replace with proper definition from Clapi codebase
molSingleton :: Ord k => k -> a -> Mol k a
molSingleton k a = Mol.fromList [(k, a)]

type KindId = DataName
type AvailableKinds = Map KindId String
type TypeId = DataName
newtype NodeSeg = NodeSeg DataName deriving (Eq, Ord, Show)

data GraphControlEvent
  = GCTranspSet TransportState
  | GCTranspCue Time
  | GCNodeAdded TypeId Placeholder [[SomeWireValue]]
  | GCNodeSeriesUpdate TypeId NodeSeg DataName (Map TpId (Maybe Attributee, TimeSeriesDataOp))
  | GCConnect
        (Maybe (TypeId, NodeSeg), Int)
        (Maybe (TypeId, NodeSeg), Int)
  | GCDisconnect
        (Maybe (TypeId, NodeSeg), Int)
        (Maybe (TypeId, NodeSeg), Int)
  deriving (Eq, Show)

data LoaderControlEvent
  = LCKindLoad KindId
  | LCKindUnload KindId
  | LCTypeLoad KindId TypeId [[SomeWireValue]]
  deriving (Eq, Show)

data ApiControlEvent
  = AEGraph GraphControlEvent
  | AELoader LoaderControlEvent
  | AEDump Placeholder
  | AEUnDump Placeholder
  deriving (Eq, Show)

dependencyOrder :: ApiControlEvent -> ApiControlEvent -> Ordering
dependencyOrder a b = case (a, b) of
    (AELoader la, AELoader lb) -> case (la, lb) of
      (LCKindLoad _, LCKindLoad _) -> EQ
      (LCKindLoad _, _) -> LT
      (_, LCKindLoad _) -> GT
      (LCTypeLoad {}, LCTypeLoad {}) -> EQ
      (LCTypeLoad {}, _) -> LT
      (_, LCTypeLoad {}) -> GT
      _ -> EQ
    (AELoader _, _) -> LT
    (_, AELoader _) -> GT
    (AEGraph ga, AEGraph gb) -> gdep ga gb
    (_, AEGraph _) -> LT
    (AEGraph _, _) -> GT
    _ -> EQ
  where
    gdep ga gb = case (ga, gb) of
      (GCNodeAdded {}, GCNodeAdded {}) -> EQ
      (GCNodeAdded {}, _) -> LT
      (_, GCNodeAdded {}) -> GT
      (GCNodeSeriesUpdate {}, GCNodeSeriesUpdate {}) -> EQ
      (GCNodeSeriesUpdate {}, _) -> LT
      (_, GCNodeSeriesUpdate {}) -> GT
      _ -> EQ

data GraphUpdateEvent
  = GUTranspSet TransportState Time
  | GUTranspCue Time
  | GUNodeAdded TypeId NodeSeg Int Int (Map DataName (TimeSeries [SomeWireValue]))
  | GUNodeSeriesChanged TypeId NodeSeg (Map DataName
        (Map TpId (Maybe Attributee, TimeSeriesDataOp)))
  | GUGlobalOutAdded Int
  | GUGlobalInAdded Int
  | GUConnected
        (Maybe (TypeId, NodeSeg), Int)
        (Maybe (TypeId, NodeSeg), Int)
  | GUDisconnected
        (Maybe (TypeId, NodeSeg), Int)
        (Maybe (TypeId, NodeSeg), Int)

data LoaderError
  = LEKindLoad KindId String
  | LEKindNotFound KindId
  | LETypeLoad KindId TypeId String
  deriving (Eq, Show)

data LoaderUpdateEvent
  = LUAvailableKinds AvailableKinds
  -- FIXME: If the ClapiReadyOpts thing works out nicely then this should be
  -- one too:
  | LUKindLoaded KindId [TupleDef]
  | LUKindUnloaded KindId
  | LUTypeLoaded KindId TypeId (Map DataName [SomeWireValue]) ClapiReadyOpts
  | LUError LoaderError

data ApiUpdateEvent
  = AUGraph GraphUpdateEvent
  | AULoader LoaderUpdateEvent
  | AUDumped DataName

arrayS, atS, changedS, cueS, elemsS, engineS, kindsS, loadedS, paramsS, pointS
  , stateS, structS, transportS, typesS, valueS, externS, inS, insS, inRefS
  , outS, outsS, outRefS, connectionsS, dumpS, noDataS :: Name a
arrayS = [n|array|]
atS = [n|at|]
changedS = [n|changed|]
cueS = [n|cue|]
elemsS = [n|elems|]
engineS = [n|engine|]
kindsS = [n|kinds|]
loadedS = [n|loaded|]
paramsS = [n|params|]
pointS = [n|point|]
stateS = [n|state|]
structS = [n|struct|]
transportS = [n|transport|]
typesS = [n|types|]
valueS = [n|value|]
externS = [n|external|]
inS = [n|in|]
insS = [n|ins|]
inRefS = [n|inref|]
outS = [n|out|]
outsS = [n|outs|]
outRefS = [n|outref|]
connectionsS = [n|connections|]
dumpS = [n|dump|]
unDumpS = [n|undump|]
noDataS = [n|nodata|]

kindsBaseSeg, typesBaseSeg :: Name a
kindsBaseSeg = [n|_kinds|]
typesBaseSeg = [n|_types|]

engineN :: Namespace
engineN = castName engineS

tsPath, tcPath, tuPath :: Path
tsPath = Root :/ transportS :/ stateS
tcPath = Root :/ transportS :/ changedS
tuPath = Root :/ transportS :/ cueS

wvEnum :: (MonadFail m, Enum a) => SomeWireValue -> m a
wvEnum wv = toEnum . fromIntegral @Word32 <$> castWireValue wv

enumWv_ :: Enum a => a -> SomeWireValue
enumWv_ = someWv WtWord32 . fromIntegral . fromEnum

type MatchResult = Maybe (String, String, String, [String])

-- FIXME: Replace this with something less hacky:
pathAsPort :: String -> Text -> Maybe (Maybe (TypeId, NodeSeg), Int)
pathAsPort inOrOut txt = case pStr =~~ ("/elems/([^/])+/([^/]+)/connections/" ++ inOrOut ++ "s/([^/]+)") :: MatchResult of
    Just (_, _, _, [outTypeName, outElem, outPortName]) -> (,)
        <$> ((\es nds -> Just (es, NodeSeg nds))
            <$> mkName (T.pack outTypeName)
            <*> mkName (T.pack outElem))
        <*> outportIdx outPortName
    Nothing -> case pStr =~~ ("/connections/" ++ inOrOut ++ "s/([^/])+") :: MatchResult of
        Just (_, _, _, [outPortName]) -> (Nothing,) <$> outportIdx outPortName
        Nothing -> Nothing
  where
    pStr = T.unpack txt
    outportIdx outPortName = mkName (T.pack outPortName) >>= flip List.elemIndex outNames

pathAsInPort = pathAsPort "in"
pathAsOutPort = pathAsPort "out"

splitSeg :: Name a -> [Name a]
splitSeg = fmap (fromJust . mkName) . T.splitOn "_" . unName

root :: Router 'RtContainer 'ForNamespace [ApiControlEvent]
root = fromList
    [ r engineN $ fromList
      [ r kindsS $ captureRouter $ \kindId -> sr $ fromList
        [ r loadedS $ leafRouter $ e $
          withSingleWv $ \wv -> toKindLoad kindId <$> wvEnum wv
        ]
      , r transportS $ fromList
        [ r cueS $ leafRouter $ e $
            withSingleWv $ \wv -> AEGraph . GCTranspCue <$> castWireValue wv
        , r stateS $ leafRouter $ e $
            withSingleWv $ \wv -> AEGraph . GCTranspSet <$> wvEnum wv
        ]
      , r typesS $ captureCreateRouter $
          \kindId ph _att crOp -> return [
            AELoader $ LCTypeLoad kindId (castName ph) (ocArgs crOp)]
      , r elemsS $ captureRouter $
          \tis -> sr $
            (captureRouter $ \iis -> sr $ fromList
              [ r paramsS $ captureLeafRouter (\p ps dc -> case dc of
                    TimeChange ops -> return [AEGraph $ GCNodeSeriesUpdate tis (NodeSeg iis) ps ops])
              , conHandler (Just (tis, NodeSeg iis)) $ Root :/ elemsS :/ tis :/ iis
              ])
            ! (CreateHandler $ (\typeId ph _att crOp -> return [AEGraph $ GCNodeAdded typeId ph $ ocArgs crOp]) tis)
      , conHandler Nothing Root
      , r dumpS $ nullContainerRouter ! (CreateHandler $ \ph ma crOp -> return [AEDump ph])
      -- FIXME: Should be a ref thing not this shonky tat:
      , r unDumpS $ nullContainerRouter ! (CreateHandler $ \ph ma crOp -> return [AEUnDump ph])
      ]
    ]
  where
    sr = SomeRouter

    r :: i1 -> Router rt i2 a -> (i1, SomeRouter i2 a)
    r i router = (i, sr router)

    e :: (DataChange -> Either Text a) -> Path -> DataChange -> Either Errors a
    e f p = fmap (first $ pathError p) f

    toKindLoad kindId shouldBeLoaded = AELoader $ if shouldBeLoaded
      then LCKindLoad kindId
      else LCKindUnload kindId

    conHandler knownNid parentPath = r connectionsS $ fromList
      [ r outsS $ conArrayHandler id pathAsInPort knownNid $ parentPath :/ connectionsS :/ outsS
      , r insS $ conArrayHandler flip pathAsOutPort knownNid $ parentPath :/ connectionsS :/ insS
      ]
    conArrayHandler evtTrans coPortParse knownNid selfPath = structRouter $ \portSeg ->
      let portPath = selfPath :/ portSeg
      in Just $ sr $ nullContainerRouter
        ! (CreateHandler $ \_ph _att crOp ->
          case (List.elemIndex portSeg outNames, ocArgs crOp) of
              (Just i, [[wv]]) -> first (pathError portPath . T.pack)
                $ (maybe [] (pure . AEGraph . evtTrans GCConnect (knownNid, i)) . coPortParse) <$> castWireValue wv
              _ -> Left $ pathError portPath "Invalid args")
        ! (CopHandler $ \epn _att op -> case op of
          SoAbsent -> case epn of
            Left _ph -> Left $ pathError portPath "Tried to remove a placeholder"
            Right cSeg ->
              let
                pSegIdx = flip List.elemIndex outNames
                (mNid, mTgtPseg) = case splitSeg cSeg of
                    [tid, nid, pSeg] -> (Just (tid, NodeSeg nid), Just pSeg)
                    [pSeg] -> (Nothing, Just pSeg)
                    _ -> (Nothing, Nothing)
                mTgtPidx = mTgtPseg >>= pSegIdx
              in case (pSegIdx portSeg, mTgtPidx) of
                (Just oIdx, Just iIdx) -> Right $ pure $ AEGraph $ evtTrans GCDisconnect (knownNid, oIdx) (mNid, iIdx)
                _ -> Left $ pathError portPath "Invalid deletion target"
          )

withSingleWv :: (SomeWireValue -> a) -> DataChange -> Either Text a
withSingleWv f dc = case dc of
  ConstChange _ma wvs -> case wvs of
    [wv] -> return $ f wv
    _ -> Left "Bad number of args"
  TimeChange {} -> Left "Not a time series"

pathError :: Path -> Text -> Mol DataErrorIndex Text
pathError p t = Mol.singleton (PathError p) t

data PreTrpd = PreTrpd {
    ptrpdPostDefs :: Map PostDefName (DefOp PostDefinition),
    ptrpdDefinitions :: Map DefName (DefOp SomeDefinition),
    ptrpdData :: DataDigest,
    ptrpdContOps :: ContOps DataName,
    ptrpdErrors :: Mol DataErrorIndex Text
}

instance Semigroup PreTrpd where
  (PreTrpd pd1 t1 d1 co1 e1) <> (PreTrpd pd2 t2 d2 co2 e2) =
    PreTrpd
        (pd1 <> pd2)
        (t1 <> t2)
        (d1 <> d2)
        (Map.unionWith (<>) co1 co2)
        (e1 <> e2)

instance Monoid PreTrpd where
  mempty = PreTrpd mempty mempty mempty mempty mempty

toTrpd :: Namespace -> PreTrpd -> TrpDigest
toTrpd ns (PreTrpd pd defs d co e) = Trpd ns pd defs d co e

eventToClapi
  :: ApiUpdateEvent -> State ExistingStuff (PreTrpd, [ApiControlEvent])
eventToClapi e = case e of
    AUGraph ge -> pure
      ( mempty
        { ptrpdData = AL.fromList $ graphEventToDcps ge
        , ptrpdContOps = graphEventToContOps ge
        }
      , [])
    AULoader le -> fmap (fmap AELoader) <$> loaderEventToClapi le
    AUDumped n -> pure
      ( mempty
          {
            ptrpdData = AL.singleton (Root :/ dumpS :/ n) $ ConstChange Nothing []
          }
      , [])

outNames :: [DataName]
outNames = [[n|L|], [n|R|]] ++ repeat [n|ohno|]

graphEventToDcps :: GraphUpdateEvent -> [(Path, DataChange)]
graphEventToDcps gue = case gue of
    GUTranspSet ts tc ->
      [ (tsPath, ConstChange Nothing [enumWv_ ts])
      , (tcPath, ConstChange Nothing [someWireable tc])
      ]
    GUTranspCue t -> [ (tuPath, ConstChange Nothing [someWireable t]) ]
    GUNodeAdded tid nodeSeg _ _ tss ->
        (\(paramSeg, ts) -> (paramPathBase tid nodeSeg :/ paramSeg, oppifyTimeSeries ts))
        <$> Map.toList tss
    GUNodeSeriesChanged tid nodeSeg changeSeries ->
        (\(paramSeg, cs) -> (paramPathBase tid nodeSeg :/ paramSeg, TimeChange cs))
        <$> Map.toList changeSeries
    GUGlobalOutAdded _ -> []
    GUGlobalInAdded _ -> []
    GUConnected (mOutElem, outPort) (mInElem, inPort) ->
      let
        conBase = maybe Root (uncurry nodePath)
        outPortSeg = outNames!!outPort
        inPortSeg = outNames!!inPort
        outPath = conBase mOutElem :/ connectionsS :/ outsS :/ outPortSeg
        outConSeg = case mInElem of
            Nothing -> inPortSeg
            Just (tid, NodeSeg nid) -> tid <> nid <> inPortSeg
        inPath = conBase mInElem :/ connectionsS :/ insS :/ inPortSeg
        inConSeg = case mOutElem of
            Nothing -> outPortSeg
            Just (tid, NodeSeg nid) -> tid <> nid <> outPortSeg
      in
        [ (outPath :/ outConSeg, ConstChange Nothing [someWv WtString $ toText unName inPath])
        , (inPath :/ inConSeg, ConstChange Nothing [someWv WtString $ toText unName outPath])
        ]
    GUDisconnected _ _ -> []
  where
    nodePath tid (NodeSeg nodeSeg) = Root :/ elemsS :/ tid :/ nodeSeg
    paramPathBase tid nodeSeg = nodePath tid nodeSeg :/ paramsS

graphEventToContOps :: GraphUpdateEvent -> ContOps DataName
graphEventToContOps gue = case gue of
    GUGlobalOutAdded i -> Map.singleton (Root :/ connectionsS :/ outsS)
        $ Map.singleton (outNames!!i) (Nothing, SoAfter $ outPrevs!!i)
    GUGlobalInAdded i -> Map.singleton (Root :/ connectionsS :/ insS)
        $ Map.singleton (outNames!!i) (Nothing, SoAfter $ outPrevs!!i)
    GUNodeAdded tid (NodeSeg nodeSeg) nIns nOuts _ ->
      let
        p = Root :/ elemsS :/ tid :/ nodeSeg :/ connectionsS
      in Map.fromList
        [ (p :/ insS, Map.fromList $ take nIns posOps)
        , (p :/ outsS, Map.fromList $ take nOuts posOps)
        ]
    GUDisconnected (mOutElem, outPort) (mInElem, inPort) ->
      let
        conBase = maybe Root $ \(tid, NodeSeg nodeSeg) -> Root :/ elemsS :/ tid :/ nodeSeg
        outPortSeg = outNames !! outPort
        outPath = conBase mOutElem :/ connectionsS :/ outsS :/ outPortSeg
        inPortSeg = outNames !! inPort
        inPath = conBase mInElem :/ connectionsS :/ insS :/ inPortSeg
      in Map.fromList
        [ (outPath, Map.singleton inPortSeg (Nothing, SoAbsent))
        , (inPath, Map.singleton outPortSeg (Nothing, SoAbsent))
        ]
    _ -> mempty
  where
    outPrevs = Nothing : (Just <$> outNames)
    posOps = zip outNames $ (\op -> (Nothing, SoAfter op)) <$> outPrevs

type Redefinable a = Map a (Set DefName)
type LoadedKinds = Redefinable KindId
type LoadedTypes = Redefinable (KindId, TypeId)

type ExistingStuff = (LoadedKinds, LoadedTypes)

loaderEventToClapi
  :: LoaderUpdateEvent -> State ExistingStuff (PreTrpd, [LoaderControlEvent])
loaderEventToClapi lue = case lue of
    LUAvailableKinds aks -> get >>= \(loadedKinds, _) -> pure . (,[]) $ mempty {
        ptrpdData = AL.fromList $ (\k ->
          ( kindLoadedPath k
          , ConstChange Nothing [enumWv_ $ Map.member k loadedKinds]))
          <$> Map.keys aks
        }
    LUKindLoaded s rtds -> either abort commit $ rtdsToCro $ (Nothing,) <$> rtds
      where
        -- The structures that the RAGE C library loaded can work just fine, but
        -- it's possible that we can't translate the structure to something
        -- representable in the API (e.g. bad segment names). In that case we
        -- have to unload the thing we just loaded!
        abort :: String -> State ExistingStuff (PreTrpd, [LoaderControlEvent])
        abort errStr = pure
            ( mempty {ptrpdErrors = Mol.singleton
                (PathError $ kindLoadedPath s) $ T.pack errStr}
            , [LCKindUnload s])
        commit
          :: ClapiReadyOpts -> State ExistingStuff (PreTrpd, [LoaderControlEvent])
        commit cro = do
            (existingLoadedKinds, loadedTypes) <- get
            let loadedKinds = Map.insert s typeSegs existingLoadedKinds
            put (loadedKinds, loadedTypes)
            pure
              ( mempty
                  { ptrpdData = AL.singleton (kindLoadedPath s) trueBool
                  , ptrpdPostDefs = Map.singleton kindSeg (OpDefine $ croToPd cro)
                  , ptrpdDefinitions = Map.fromList $ tupSdOps ++
                      [ ( typesS
                        , OpDefine $ typesStructRedef loadedKinds)
                      , (kindArraySeg, kindArrayDef)
                      ]
                  }
              , [])
          where
            tupSdOps :: [(DefName, DefOp SomeDefinition)]
            tupSdOps = fmap OpDefine <$>
              (croToStructDef ReadOnly cro kindStructSeg)
            typeSegs = Set.fromList $
              [kindArraySeg] ++ (fst <$> tupSdOps)
            typesStructRedef kinds = structDef "All instances of loaded kinds" $
              AL.fromMap $ Map.mapWithKey
                (\k _v -> (kindsBaseSeg <> castName k <> arrayS, Editable))
                kinds
            kindSeg, kindStructSeg :: Name a
            kindSeg = kindsBaseSeg <> castName s
            kindStructSeg = kindSeg <> structS
            kindArraySeg = kindSeg <> arrayS
            kindArrayDef = OpDefine $ arrayDef "Type array"
              (Just kindSeg)
              kindStructSeg ReadOnly
            trueBool = ConstChange Nothing [enumWv_ True]
    LUTypeLoaded ks ts kps cro -> do
        (loadedKinds, existingLoadedTypes) <- get
        let loadedTypes = Map.insert (ks, ts) elemSegs existingLoadedTypes
        put (loadedKinds, loadedTypes)
        pure
          ( mempty
            { ptrpdData = AL.fromMap $ Map.mapKeys (basePath :/) $
                ConstChange Nothing <$> kps
            , ptrpdPostDefs = Map.singleton typeSeg (OpDefine $ croToPd cro)
            , ptrpdDefinitions = Map.fromList $ tupSdOps ++
              [ (elemsS, OpDefine $ elemsStructRedef loadedTypes)
              , (typeArraySeg, typeArrayDef)
              , (typeStructSeg, typeStructDef)
              ]
            }
          , [])
      where
        basePath = Root :/ typesS :/ ks :/ ts
        tupSdOps = fmap OpDefine <$>
          (croToStructDef Editable cro typeParamsStructSeg)
        -- FIXME: The doc for this should flow from rage:
        typeArrayDef = OpDefine $ arrayDef "Elem array"
            (Just typeSeg) typeStructSeg Editable
        typeStructDef = OpDefine $ structDef
            "Should be from rage?" $ AL.fromList $
              [ (paramsS, (typeParamsStructSeg, ReadOnly))
              , (connectionsS, (connectionsS, ReadOnly))
              ]
        elemsStructRedef types = structDef "All instances of loaded types" $
          AL.fromMap $ Map.mapKeys snd $ Map.mapWithKey
            (\(kid, tid) _v -> (typesBaseSeg <> castName tid <> arrayS , Editable))
            types
        typeSeg, typeStructSeg, typeParamsStructSeg, typeArraySeg :: Name a
        typeSeg = typesBaseSeg <> castName ts
        typeStructSeg = typeSeg <> structS
        typeParamsStructSeg = typeStructSeg <> paramsS
        typeArraySeg = typeSeg <> arrayS
        elemSegs = Set.fromList $ typeArraySeg : typeStructSeg : (fst <$> tupSdOps)
    LUError e -> error $ show e
  where
    kindLoadedPath s = Root :/ kindsS :/ s :/ loadedS

apiDefs :: Map DefName SomeDefinition
apiDefs = Map.fromList
  [ (engineS, localStruct "namespace" $ (dumpS, dumpS, Editable) : (unDumpS, unDumpS, Editable) :
      (fmap (\s -> (s, castName s, ReadOnly)) $
        [ transportS
        , kindsS
        , typesS
        , elemsS
        , connectionsS
        ]))
  , (transportS, localStruct "transport" $
      [ (stateS, [n|transport_state|], Editable)
      , (changedS, [n|transport_changed|], ReadOnly)
      , (cueS, [n|transport_cuepoint|], Editable)
      ])
  , ([n|transport_state|], tupleDef
      "playhead"
      (AL.singleton stateS $ ttEnum' (Proxy @TransportState))
      Nothing)
  , ([n|transport_cuepoint|], tupleDef
      "where the transport starts in session time"
      (AL.singleton pointS ttTime)
      Nothing)
  , ([n|transport_changed|], tupleDef
      "where the transport last changed in engine wall clock time"
      (AL.singleton atS ttTime)
      Nothing)
  , ([n|kind_loaded|], tupleDef
      "Should the kind be loaded"
      (AL.singleton valueS $ ttEnum' (Proxy :: Proxy Bool))
      Nothing)
  , ([n|kind_info|], localStruct "Stuff about the kind"
      [ (loadedS, [n|kind_loaded|], Editable) ])
  , (kindsS, arrayDef
      "The available kinds of things"
      Nothing
      [n|kind_info|]
      ReadOnly)
  , (typesS, structDef "Types (parameterised kinds)" mempty)
  , (elemsS, structDef "Elements (instances of types)" mempty)
  , (connectionsS, localStruct "Connections for something"
      [ (insS, insS, ReadOnly)
      , (outsS, outsS, ReadOnly)
      ])
  , (insS, arrayDef "Inputs" Nothing inS Editable)
  , (inS, arrayDef "An input" (Just outRefS) outRefS ReadOnly)
  , (outRefS, tupleDef "Connection to an output" (AL.singleton outS $ ttRef outS) Nothing)
  , (outsS, arrayDef "Outputs" Nothing outS Editable)
  , (outS, arrayDef "An output" (Just inRefS) inRefS ReadOnly)
  , (inRefS, tupleDef "Connection to an input" (AL.singleton inS $ ttRef inS) Nothing)
  , (dumpS, arrayDef "Savepoints" (Just noDataS) noDataS ReadOnly)
  , (unDumpS, arrayDef "Loadpoints" (Just noDataS) noDataS ReadOnly)
  , (noDataS, tupleDef "Triggers" mempty Nothing)
  ]
  where
    localStruct doc = structDef doc .
        AL.fromList . fmap (\(ks, ts, l) -> (ks, (ts, l)))

apiPostDefs :: Map PostDefName PostDefinition
apiPostDefs = Map.fromList
  [ (outRefS, PostDefinition "Connect to this output" $ AL.singleton outS [ttRef outS])
  , (inRefS, PostDefinition "Connect to this input" $ AL.singleton inS [ttRef inS])
  , (noDataS, PostDefinition "Triggers" mempty)
  ]

initialDd :: Time -> DataDigest
initialDd t = AL.fromList $
  [ (tsPath, ConstChange Nothing [enumWv_ TransportStopped])
  , (tcPath, ConstChange Nothing [someWireable t])
  , (tuPath, ConstChange Nothing [someWireable $ Time 0 0])
  ]

skipIfEmpty :: (Foldable f, Monad m) => (f a -> m ()) -> f a -> m ()
skipIfEmpty action b = unless (null b) $ action b

publishEvents :: [ApiUpdateEvent] -> ExistingStuff -> (ExistingStuff, TrpDigest)
publishEvents evts st =
  let
    (rv, st') = runState (sequence $ eventToClapi <$> evts) st
    (ptrpds, aces) = unzip rv
  in (st', toTrpd engineN $ mconcat ptrpds)

frpDigestToEvents :: SomeFrDigest -> (Errors, [ApiControlEvent])
frpDigestToEvents (SomeFrDigest d) = case d of
    Frpd {} -> handleFrpDigest root d
    Frcrd _ -> mempty
    Frped f -> error $ "Received error digest from relay\n" ++ show f
    _ -> error $ "Received unexpected digest type from relay: " ++ show d

initialDigest :: Time -> TrpDigest
initialDigest startTime = (trpdEmpty engineN)
  { trpdDefs = OpDefine <$> apiDefs
  , trpdPostDefs = OpDefine <$> apiPostDefs
  , trpdData = initialDd startTime
  }


data DataDump
  = ConstSet (Maybe Attributee) [SomeWireValue]
  -- FIXME: Put interpolation before wvs:
  | Tps (Map TpId (Maybe Attributee, Time, [SomeWireValue], Interpolation))

data TreeDump
  = TreeDump
  { tdDefs :: Map DefName SomeDefinition
  , tdData :: AssocList Path DataDump
  }

-- FIXME: Rename this
asTvs :: TreeDump -> Snapshot
asTvs (TreeDump defs dta) = Map.mapWithKey asLs $ AL.toMap dta
  where
    asLs :: Path -> DataDump -> LeafSnapshot
    asLs p =
      let
        tts = ptt engineS p
      in \case
        ConstSet ma wvs -> LsSingle ma $ zipWith tyAlign tts $ Identity <$> wvs
        Tps pts ->
          let
            splitPt (atis, wvss) (ma, t, wvs, i) = ((ma, t, i) : atis, wvs : wvss)
            (atis, wvss) = foldl splitPt mempty pts
            svs = zipWith tyAlign tts $ reverse <$> List.transpose wvss
          in LsSeries (reverse atis) svs

    tyAlign :: Functor f => SomeTreeType -> f SomeWireValue -> TypedValues f
    tyAlign (SomeTreeType tt) wvs = TypedValues tt $ wvAlign tt <$> wvs
    wvAlign :: TreeType a -> SomeWireValue -> a
    wvAlign tt (SomeWireValue (WireValue wt b)) = case typeValid wt tt of
        Just Refl -> case inflateValue tt b of
            Nothing -> error "Uninflatable?"
            Just v -> v
        Nothing -> error "Tree and wire values differ during dump"

    allDefs = defs <> apiDefs
    ptt :: DefName -> Path -> [SomeTreeType]
    ptt dn = \case
        (n :</ p) -> case getDef dn of
            SomeDefinition d -> case getTyInfoForName n d of
                Nothing -> error "Path followed missing child type"
                Just (cdn, _) -> ptt cdn p
        _ -> case getDef dn of
            SomeDefinition td@(TupleDef {}) -> AL.values $ tupDefTys td
            _ -> error "Data at non-leaf node"
    getDef n = maybe (error "Def missing during dump") id $ Map.lookup n allDefs
