module Util where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Map.Merge.Strict (merge, mapMissing, zipWithMatched)

mapDiffEvents
  :: Ord k => (k -> a -> [evt])
  -> (k -> b -> [evt]) -> (k -> a -> b -> [evt])
  -> Map k a -> Map k b -> [evt]
mapDiffEvents onlyA onlyB both ma mb = concat $ Map.elems $ merge
    (mapMissing onlyA) (mapMissing onlyB) (zipWithMatched both) ma mb

-- FIXME: I think these helpers are a sign that the data structures they are
-- used on are the wrong shape:
updateIdx :: (a -> a) -> Int -> [a] -> [a]
updateIdx f idx l = pre ++ [f a] ++ post
  where
    (pre, a : post) = splitAt idx l

replaceIdx :: a -> Int -> [a] -> [a]
replaceIdx = updateIdx . const
