{-# LANGUAGE
   TupleSections, TypeApplications, GADTs, LambdaCase, TypeFamilies
  , DataKinds, QuasiQuotes, PatternSynonyms
#-}

module ClapiEventApplier where

import Data.Functor.Identity (runIdentity)
import Data.List (sortBy)
import Data.Maybe (fromJust, mapMaybe)
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Text as T
import Data.Tuple (swap)
import Data.List (transpose)
import Data.Word (Word32)
import Control.Monad (foldM, unless)
import Control.Monad.IO.Class (MonadIO(liftIO))
import System.FilePath (takeBaseName)
import Data.Type.Equality ((:~:)(Refl), testEquality)

import Clapi.Protocol (Protocol, waitThen, sendFwd, sendRev)
import Clapi.Types
  ( SomeWireValue, unUniqList
  , TpId, Attributee(..), TimeSeriesDataOp(..)
  , Time(..), Interpolation(IConstant), TrDigest(..) , TrDigest(..), trpdEmpty
  , SomeTrDigest(..), FrpDigest, TrpDigest, DefOp(..), SomeDefinition
  , DataChange(..), DataDigest, FrDigest(Frpd), OriginatorRole(Provider)
  , DigestAction(Update), SomeFrDigest(..), Creates, CreateOp(..), Placeholder
  , Path)
import Clapi.Types.Name (DataName, TupMemberName, castName, unName, mkName)
import Clapi.Types.Path (pattern Root, pattern (:/))
import Clapi.Tree (TimeSeries)
import Clapi.Types.AssocList (AssocList)
import qualified Clapi.Types.EnumVal as EV
import qualified Clapi.Types.AssocList as AL
import qualified Clapi.Types.Dkmap as Dkmap
import Rage.Loader
  ( Kind(paramDefs), Type, loadKind , typeInstanceSpec
  , InstanceSpec(isControls), mkType)
import Clapi.Types.Wire (SomeWireValue(..), WireValue(..), WireType(..))
import Clapi.Types.Tree (WireTypeOf, TreeType(..))

import Clapi.Snapshot.File (TypedValues(..), SeriesValue, LeafSnapshot(..))
import Clapi.Snapshot.Snapshot (writeSnapshot, readSnapshot, Snapshot)
import DefConv (translateValues, rtdsToCro, ClapiReadyOpts, itdPair)
import Cape.Types.Session
  ( Session(..), ModifiedSession, TransportInfo(..), NodeName(..)
  , newSession, TouchedPoints, TouchedStuff(..), AudioGraph)
import ClapiTranslator
  ( ApiControlEvent(..), ApiUpdateEvent(..), GraphControlEvent(..)
  , GraphUpdateEvent(..), LoaderUpdateEvent(..), LoaderControlEvent(..)
  , LoaderError(..), KindId, TypeId, NodeSeg(..), AvailableKinds, TreeDump(..)
  , publishEvents, frpDigestToEvents, initialDigest, engineN, skipIfEmpty
  , asTvs, DataDump(..), dependencyOrder, typesS, elemsS)
import Util (mapDiffEvents, updateIdx)
import Cape.Types.Graph
  ( Graph(..), Node(..), addNode, adjustNode, OutPortId(..)
  , InPortId(..), connect, disconnect)
import Cape.Types.Element (Element(..))
import RageGraphSync (rTup)

type Kinds = Map KindId Kind
type InstanceSegMap = Map NodeName (KindId, TypeId, NodeSeg, ClapiReadyOpts)
data LoaderResults = LoaderResults
  { lrAvailableKinds :: AvailableKinds
  , lrKinds :: Kinds
  , lrTypes :: Map (KindId, TypeId) (Type, AssocList TupMemberName [SomeWireValue])
  -- FIXME: Not sure this should really be here, or maybe the structure is
  -- wrong and it should be?
  , lrInstanceSegMap :: InstanceSegMap
  }

loaderResultsEmpty :: LoaderResults
loaderResultsEmpty = LoaderResults mempty mempty mempty mempty

-- FIXME: This doesn't check the validity of what it passes through (and bad
-- data past here can cause segfaults) also doesn't do attribution (also
-- there's no Dkmap.singleton):
asTimeSeries :: ClapiReadyOpts -> [[SomeWireValue]] -> [TimeSeries [SomeWireValue]]
asTimeSeries _cro initVals = (\val -> fromJust $ Dkmap.insert 0 (Time 0 0) (Nothing, (IConstant, val)) Dkmap.empty) <$> initVals

applyGraphEvent :: TouchedStuff -> GraphControlEvent -> LoaderResults -> Session -> (TouchedStuff, InstanceSegMap, Session)
applyGraphEvent touchedStuff (GCTranspSet ts) lr aps =
  ( touchedStuff
  , lrInstanceSegMap lr
  , aps {apsTransp = (apsTransp aps) {tiState = ts}}
  )
applyGraphEvent touchedStuff (GCTranspCue t) lr aps =
  ( touchedStuff
  , lrInstanceSegMap lr
  , aps {apsTransp = (apsTransp aps) {tiCue = t}}
  )
applyGraphEvent touchedStuff (GCNodeAdded tid ph initVals) lr aps =
  let
    -- FIXME: this is nuts
    kid = fromJust $ lookup tid $ swap <$> Map.keys (lrTypes lr)
    (et, _) = fromJust $ Map.lookup (kid, tid) $ lrTypes lr
    cro = fromJust $ rtdsToCro $ itdPair <$> isControls (typeInstanceSpec et)
    nodeId = NodeName $ unName kid <> unName tid <> unName ph
    apsg = case addNode nodeId (Element et $ asTimeSeries cro initVals) $ apsGraph aps of
        Nothing -> error "Node add failed"
        Just g' -> g'
    -- FIXME: Does not check placeholder uniqueness
  in (touchedStuff, Map.insert nodeId (kid, tid, NodeSeg $ castName ph, cro) $ lrInstanceSegMap lr, aps {apsGraph = apsg})
applyGraphEvent touchedStuff (GCNodeSeriesUpdate _tid nodeSeg seriesSeg tsOps) lr aps =
  let
    (nodeId, cro') = fromJust $ lookup nodeSeg $ (\(nid, (_, _, ns, cro'')) -> (ns, (nid, cro''))) <$> (Map.toList $ lrInstanceSegMap lr)
    applyEdit :: TimeSeries [SomeWireValue] -> TpId -> (Maybe Attributee, TimeSeriesDataOp) -> TimeSeries [SomeWireValue]
    applyEdit ts tpid (ma, tsdo) = case tsdo of
        OpSet t wvs i -> fromJust $ Dkmap.set tpid t (ma, (i, wvs)) ts
        OpRemove -> Dkmap.deleteK0 tpid ts
    seriesIdx = fromJust $ lookup (castName seriesSeg) $ zip (unUniqList $ AL.keys cro') [0..]
    editSeries nb =
        let
            newTsl = updateIdx (\s -> Map.foldlWithKey applyEdit s tsOps) seriesIdx $ timeSeries nb
        in nb {timeSeries = newTsl}
    -- FIXME: Ignores pre-existing TS
    attribMap curMap = Map.singleton seriesSeg $ Map.foldlWithKey (\m tpid (ma, _) -> Map.insert tpid ma m) mempty tsOps
    (TouchedStuff touchedMap) = touchedStuff
    touchedStuff' = TouchedStuff $ Map.alter (Just . attribMap . maybe mempty id) nodeId touchedMap
  in (touchedStuff', lrInstanceSegMap lr, aps {apsGraph = adjustNode editSeries nodeId $ apsGraph aps})
applyGraphEvent touchedStuff (GCConnect oSegs iSegs) lr aps =
  (touchedStuff, lrInstanceSegMap lr, aps {apsGraph = conLike connect oSegs iSegs lr aps})
applyGraphEvent touchedStuff (GCDisconnect oSegs iSegs) lr aps =
  (touchedStuff, lrInstanceSegMap lr, aps {apsGraph = conLike disconnect oSegs iSegs lr aps})

conLike
  :: (OutPortId NodeName -> InPortId NodeName -> AudioGraph -> AudioGraph)
  -> (Maybe (DataName, NodeSeg), Int) -> (Maybe (DataName, NodeSeg), Int)
  -> LoaderResults -> Session -> AudioGraph
conLike action (mOutN, outP) (mInN, inP) lr aps =
  let
    asPortId (_tid, nSeg) = fromJust $ lookup nSeg $ (\(nid, (_, _, ns, _)) -> (ns, nid)) <$> (Map.toList $ lrInstanceSegMap lr)
    opid = OutPortId $ (asPortId <$> mOutN, fromIntegral outP)
    ipid = InPortId $ (asPortId <$> mInN, fromIntegral inP)
  in action opid ipid $ apsGraph aps

applyLoaderEvent
  :: LoaderControlEvent -> LoaderResults
  -> IO (Either LoaderError LoaderResults)
applyLoaderEvent lce ls = case lce of
    LCKindLoad kid -> if Map.member kid $ lrKinds ls
        then pure $ Right ls
        else case Map.lookup kid $ lrAvailableKinds ls of
            Nothing -> pure $ Left $
              LEKindLoad kid "Not one of the available kinds"
            Just kindStr -> wrapUp kid <$> loadKind kindStr
    LCKindUnload kid -> pure $ Right $
      ls {lrKinds = Map.delete kid $ lrKinds ls}
    LCTypeLoad kid tid kps ->
      case Map.lookup kid $ lrKinds ls of
        Nothing -> return $ Left $ LEKindNotFound kid
        Just kind ->
          case translateValues (fromJust $ rtdsToCro $ (Nothing,) <$> paramDefs kind) kps of
            Left errStr -> return $ Left $ LETypeLoad kid tid errStr
            Right values -> do
              failyElementType <- mkType kind $ fmap rTup $ AL.values values
              return $ case failyElementType of
                Left errStr -> Left $ LETypeLoad kid tid errStr
                Right elemType -> Right $
                  -- FIXME: defaultless kps:
                  ls {lrTypes = Map.insert (kid, tid) (elemType, fromJust $ AL.mapKeys castName values) $
                        lrTypes ls}
  where
    wrapUp kid mfk = case mfk of
        Left msg -> Left $ LEKindLoad kid msg
        Right k -> Right $ ls {lrKinds = Map.insert kid k $ lrKinds ls}

type Dumps = [FilePath]

applyApiEvent
  :: (TouchedStuff, [LoaderError], LoaderResults, Session, Dumps) -> ApiControlEvent
  -> IO (TouchedStuff, [LoaderError], LoaderResults, Session, Dumps)
applyApiEvent (touchedStuff, les, ls, aps, dumps) e = case e of
    AEGraph ge ->
      let
        (touchedStuff', ism, aps') = applyGraphEvent touchedStuff ge ls aps
      in pure $ (touchedStuff', les, ls {lrInstanceSegMap = ism}, aps', dumps)
    AELoader le ->
        either (\lerr -> (touchedStuff, lerr : les, ls, aps, dumps)) (\ls' -> (touchedStuff, les, ls', aps, dumps))
        <$> applyLoaderEvent le ls
    AEDump ph -> let dumpP = "/tmp/capedump/" ++ (T.unpack $ unName ph) in do
        snap dumpP ls aps
        pure (touchedStuff, les, ls, aps, dumpP : dumps)
    AEUnDump ph -> let dumpP = "/tmp/capedump/" ++ (T.unpack $ unName ph) in do
        (ls', aps') <- unSnap (lrAvailableKinds ls) dumpP
        pure (mempty, mempty, ls', aps', dumps)

tsBySeg :: ClapiReadyOpts -> Element -> Map TupMemberName (TimeSeries [SomeWireValue])
tsBySeg cro ei = Map.fromList $ zip (fmap castName $ unUniqList $ AL.keys cro) (timeSeries ei)

attributedTouchyThings
  :: ClapiReadyOpts -> Element -> TouchedPoints
  -> Map DataName (Map TpId (Maybe Attributee, TimeSeriesDataOp))
attributedTouchyThings cro ei = Map.mapWithKey opify
  where
    opify s tops = case Map.lookup (castName s) tss of
        Nothing -> error "attempting to attribute non-existant seg"
        Just ts -> Map.fromList $ Map.elems $ Map.mapWithKey (asOp ts) tops
    -- FIXME: There isn't another exposed method for doing this that I can
    -- find, which is suboptimal:
    asOp ts tpid ma = case Map.lookup tpid $ Dkmap.flatten (,) ts of
        Nothing -> (tpid, (ma, OpRemove))  -- FIXME: What if it wasn't there before!
        Just (t, (att, (i, wvs))) -> (tpid, (att, OpSet t wvs i))
    tss = tsBySeg cro ei

graphDiffEvents :: InstanceSegMap -> TouchedStuff -> Session -> Session -> [GraphUpdateEvent]
graphDiffEvents segMap (TouchedStuff touchedPoints) a a' =
  let
    ti = apsTransp a
    ti' = apsTransp a'
    msm = if tiState ti == tiState ti'
      then []
      else [GUTranspSet (tiState ti') (tiChanged ti')]
    ccm = if tiCue ti == tiCue ti'
      then []
      else [GUTranspCue $ tiCue ti']
    nodeEvts = mapDiffEvents
        undefined
        (\nid n -> case Map.lookup nid segMap of
            Nothing -> error "Node not known to clapi translator"
            Just (_kid, tid, nodeSeg, paramCro) ->
                pure $ GUNodeAdded
                    tid nodeSeg
                    (length $ nodeInputs n) (length $ nodeOutputs n)
                    $ Map.mapKeys castName $ tsBySeg paramCro $ nodeBody n
        )
        (\nid _ n' -> case Map.lookup nid segMap of
            Nothing -> error "Node not know to clapi translator on update"
            Just (_kid, tid, nodeSeg, paramCro) -> pure $ GUNodeSeriesChanged tid nodeSeg $
                attributedTouchyThings paramCro (nodeBody n') $ Map.findWithDefault Map.empty nid touchedPoints)
        (graphNodes $ apsGraph a)
        (graphNodes $ apsGraph a')
    outEvts = take
        (length (graphOutputs $ apsGraph a') - length (graphOutputs $ apsGraph a))
        $ GUGlobalOutAdded <$> [0..]
    inEvts = take
        (length (graphInputs $ apsGraph a') - length (graphInputs $ apsGraph a))
        $ GUGlobalInAdded <$> [0..]
    toNsegTid mNid = case mNid of
        Nothing -> Nothing
        Just nid -> case Map.lookup nid segMap of
            Nothing -> error "Connected node not known to clapi translator"
            Just (_, tid, nodeSeg, _) -> Just (tid, nodeSeg)
    consOnlyIn action tgt excl =
        (\(OutPortId (outNid, outP), InPortId (inNid, inP)) -> action
            (toNsegTid outNid, fromIntegral outP)
            (toNsegTid inNid, fromIntegral inP))
        <$> Set.toList (Set.difference (graphConnections $ apsGraph tgt) $ graphConnections (apsGraph excl))
    connEvts = consOnlyIn GUConnected a' a
    disconnEvts = consOnlyIn GUDisconnected a a'
  in msm ++ ccm ++ nodeEvts ++ outEvts ++ inEvts ++ connEvts ++ disconnEvts

loaderResultsDiffEvents :: LoaderResults -> LoaderResults -> [LoaderUpdateEvent]
loaderResultsDiffEvents a a' = akEvents ++ klEvents ++ tlEvents
  where
    klEvents = mapDiffEvents unloaded loaded common (lrKinds a) (lrKinds a')
    loaded s k = pure $ LUKindLoaded s $ paramDefs k
    unloaded s _ = [LUKindUnloaded s]
    common s k k' = if k == k'
        then []
        else []
        -- FIXME: see https://gitlab.com/concert/cape/issues/18.
        -- else [LUError $ LEKindLoad s "Unexpected reload"]
    akEvents = if lrAvailableKinds a /= lrAvailableKinds a'
        then [LUAvailableKinds $ lrAvailableKinds a']
        else []
    tlEvents = mapDiffEvents tyUnload tyLoad tyCom (lrTypes a) (fmap (fromJust . AL.mapKeys castName) <$> lrTypes a')
    tyUnload = undefined
    -- FIXME: Not sure this failure mode belongs here?
    tyLoad (kid, tid) (ty, kps) = case typeCro ty of
        Just cro -> pure $ LUTypeLoaded kid tid (AL.toMap kps) cro
        Nothing -> [LUError $ LETypeLoad kid tid "Unable to turn type into cro"]
    -- FIXME: Implement this properly:
    tyCom _ _ _ = []

typeCro :: Type -> Maybe ClapiReadyOpts
typeCro ty = rtdsToCro $ itdPair <$> isControls (typeInstanceSpec ty)

eventsFromEmpty :: LoaderResults -> Session -> [ApiUpdateEvent]
eventsFromEmpty lr s =
    (AUGraph <$> graphDiffEvents
        (lrInstanceSegMap lr) (TouchedStuff Map.empty)
        newSession s) ++
    (AULoader <$> loaderResultsDiffEvents loaderResultsEmpty lr)

dumpDiffEvents :: Dumps -> Dumps -> [DataName]
dumpDiffEvents d d' = finalSegName <$> consed
  where
    consed = take (length d' - length d) d'
    finalSegName = fromJust . mkName . T.pack . takeBaseName

apiProto
  :: MonadIO m => Time -> LoaderResults -> Session
  -> Protocol
    SomeFrDigest ModifiedSession
    SomeTrDigest ModifiedSession
    m ()
apiProto startTime initialLoaderResults initialAps = do
    sendTrd $ initialDigest startTime
    let (st, initPub) = publishEvents initialStateEvents mempty
    sendTrd initPub
    go initialLoaderResults initialAps st mempty
  where
    sendTrd = sendRev . SomeTrDigest
    sendErrs e = sendTrd (trpdEmpty engineN) {trpdErrors = e}
    initialStateEvents = eventsFromEmpty initialLoaderResults initialAps
    go loaderResults aps st dumps = waitThen fwd rev
      where
        fwd d = do
            skipIfEmpty sendErrs errs
            (touchedStuff, loadErrs, loaderResults', aps', dumps') <- liftIO $
                foldM applyApiEvent (TouchedStuff Map.empty, [], loaderResults, aps, dumps) evts
            let (st', trpd) = publishEvents
                  ( (AULoader . LUError <$> loadErrs)
                    ++ (AULoader <$> loaderResultsDiffEvents loaderResults loaderResults')
                    ++ (AUDumped <$> dumpDiffEvents dumps dumps'))
                  st
            unless (trpdNull trpd) $ sendTrd trpd
            sendFwd (touchedStuff, aps')
            go loaderResults' aps st' dumps'
          where
            (errs, evts) = frpDigestToEvents d
        rev (touchedStuff, aps') = do
            unless (trpdNull trpd) $ sendTrd trpd
            go loaderResults aps' st' dumps
          where
            evts = AUGraph <$> graphDiffEvents (lrInstanceSegMap loaderResults) touchedStuff aps aps'
            (st', trpd) = publishEvents evts st
        trpdNull :: TrpDigest -> Bool
        trpdNull (Trpd _ns pds defs dd cops errs) =
            null pds && null defs && null dd && null cops && null errs


asDump :: LoaderResults -> Session -> TreeDump
asDump lr s = TreeDump defs dDump
  where
    evts = eventsFromEmpty lr s
    (_, trpd) = publishEvents evts mempty
    defs = onlyDefs <$> trpdDefs trpd
    dDump = onlyAssigns <$> trpdData trpd
    onlyDefs :: DefOp SomeDefinition -> SomeDefinition
    onlyDefs = \case
        OpDefine sd -> sd
        OpUndefine -> error "Got a def undefine whilst diffing from empty"
    onlyAssigns :: DataChange -> DataDump
    onlyAssigns = \case
        ConstChange ma wvs -> ConstSet ma wvs
        TimeChange pc -> Tps $ unpackSet <$> pc
    unpackSet (a, o) = case o of
        OpSet t w i -> (a, t, w, i)
        OpRemove -> error "Got remove whilst diffing from empty"

snap :: FilePath -> LoaderResults -> Session -> IO ()
snap fp lr s = writeSnapshot (asTvs $ asDump lr s) fp

unSnap :: AvailableKinds -> FilePath -> IO (LoaderResults, Session)
unSnap ak fp = readSnapshot fp >>= doApply . snd . frpDigestToEvents . SomeFrDigest . snapBundle
  where
    snapBundle :: Snapshot -> FrpDigest
    snapBundle s = let dd = ddIfy s in
      Frpd engineN dd (ddTc dd) mempty
    doApply :: [ApiControlEvent] -> IO (LoaderResults, Session)
    doApply evts = do
        (_, les, lr, s, _) <- foldM
          applyApiEvent
          (mempty, mempty, loaderResultsEmpty {lrAvailableKinds = ak}, newSession, mempty)
          (sortBy dependencyOrder evts)
        pure (lr, s)

ddTc :: DataDigest -> Creates
ddTc = Map.fromList . mapMaybe transform . AL.unAssocList
  where
    transform :: (Path, DataChange) -> Maybe (Path, Map Placeholder (Maybe Attributee, CreateOp))
    transform (path, dc) = case path of
      Root :/ topLevel :/ secondLevel :/ ph :/ _
        | topLevel == typesS -> case dc of
            ConstChange ma wvs -> Just (Root :/ typesS :/ secondLevel, Map.singleton (castName ph) (ma, OpCreate [wvs]))
            TimeChange _ -> error "Types cannot have timeseries"
      Root :/ topLevel :/ secondLevel :/ ph :/ _ :/ _
        | topLevel == elemsS -> case dc of
           ConstChange _ _-> error "Elements cannot be constant"
           TimeChange pts -> let (ma, wvs) = firstPoint pts in
             Just (Root :/ elemsS :/ secondLevel, Map.singleton (castName ph) (ma, OpCreate [wvs]))
      _ -> Nothing
    firstPoint :: Map Word32 (Maybe Attributee, TimeSeriesDataOp) -> (Maybe Attributee, [SomeWireValue])
    firstPoint pts =
      let
        (ma, zeroPt) = fromJust $ Map.lookup 0 pts
      in case zeroPt of
        OpSet _t wvs _i -> (ma, wvs)
        _ -> error "Remove generated during load"


ddIfy :: Snapshot -> DataDigest
ddIfy = AL.fromMap . fmap ddPath
  where
    ddPath = \case
        LsSeries atis vs -> TimeChange $ Map.fromList $ zip [0..] $ zipWith asTsdo atis $ transpose $ tvAsWvs <$> vs
        LsSingle a v -> ConstChange a $ runIdentity . tvAsWvs <$> v
    asTsdo :: (Maybe Attributee, Time, Interpolation) -> [SomeWireValue] -> (Maybe Attributee, TimeSeriesDataOp)
    asTsdo (ma, t, i) wvs = (ma, OpSet t wvs i)

tvAsWvs :: Functor f => TypedValues f -> f SomeWireValue
tvAsWvs (TypedValues tt v) = SomeWireValue . asWireValue tt <$> v

asWireValue :: TreeType a -> a -> WireValue (WireTypeOf a)
asWireValue tt a = case tt of
    TtTime -> WireValue WtTime a
    TtEnum _ -> WireValue WtWord32 $ EV.toWord32 a
    TtWord32 _ -> WireValue WtWord32 a
    TtWord64 _ -> WireValue WtWord64 a
    TtInt32 _ -> WireValue WtInt32 a
    TtInt64 _ -> WireValue WtInt64 a
    TtFloat _ -> WireValue WtFloat a
    TtDouble _ -> WireValue WtDouble a
