{-# LANGUAGE
    GADTs, DataKinds, KindSignatures, LambdaCase, RankNTypes
  , StandaloneDeriving, TupleSections, OverloadedStrings
#-}

module Rendering where

import Control.Concurrent (threadDelay)
import Control.Monad.Except (runExceptT)
import Control.Monad.State (state, execState, State, modify, get, put)
import Data.List (nub)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import qualified Data.Text as T

import Clapi.Tree (Attributed, TimePoint, TimeSeries)
import Clapi.Types.Dkmap (Dkmap)
import qualified Clapi.Types.Dkmap as Dkmap
import qualified Clapi.Types.AssocList as AL
import Clapi.Types.Wire (SomeWireValue, someWireable)
import Clapi.Types.Base (Time, TpId, Interpolation(IConstant))

import Rage.Types.Backend (SampleRate(..), PeriodSize(..))
import Rage.Loader (Type)
import Rage.Graph (TransportState(..))
import qualified Rage.Bulk as BB

import Cape.Types.Session (AudioGraph, zt, newAudioGraph, NodeName(..))
import Cape.Types.Graph (Graph(..), addNode, OutPortId(..), InPortId(..))
import Cape.Types.Element (Element(..))
import RageGraphSync
  ( applyGraph, applyTransp, RageTransportControl(RtcTranspSetState)
  , withRunningGraph, RageSessionEvent, REError, RunningGraph)
import RageEventTranslator (rageGraphControl)
import Cape.Types.AudioSource
  ( SomeAudioSource, withSomeAudioSource, Slice, Source, Sources
  , AudioSource(..), AudioSourceType(..), getDuration, takeSlice, sliceStart
  , sliceSource, sliceEnd, getChannelCount, sourceChannelCount)

import Control.Exception (try, SomeException)

newtype ExeGraph = ExeGraph AudioGraph

data PersistMode
  = Playback
  | Capture
  deriving (Enum, Eq, Ord)

active :: FilePath -> Time -> TimePoint [SomeWireValue]
active p t = (IConstant, [someWireable $ T.pack p, someWireable t])

idle :: TimePoint [SomeWireValue]
idle = active "" zt

newtype PersistType = PersistType Type

type GetPersistType = PersistMode -> Int -> IO PersistType

persistFor :: GetPersistType -> Source -> IO Element
persistFor getPTy slices = do
    PersistType pTy <- getPTy Playback $ sourceChannelCount slices
    pure $ Element pTy [cap Nothing $ foldl sliceTp (Dkmap.empty, zt) slices]
  where
    cap att (ts, endTime) = dkmInsert endTime (att, idle) ts
    sliceTp
      :: (TimeSeries [SomeWireValue], Time) -> Attributed Slice
      -> (TimeSeries [SomeWireValue], Time)
    sliceTp (ts, endTime) (att, slice) = if sliceStart slice <= endTime
        then
          ( dkmInsert
              endTime
              ( att
              , active (withSomeAudioSource getSrc $ sliceSource slice) $ sliceStart slice)
              ts
          , sliceEnd slice)
        else sliceTp (cap att (ts, endTime), sliceStart slice) (att, slice)

dkmInsert :: Ord k1 => k1 -> v -> Dkmap TpId k1 v -> Dkmap TpId k1 v
dkmInsert = dkmInsert' 0
  where
    dkmInsert' tpid t v dkm = case Dkmap.insert tpid t v dkm of
        Nothing -> dkmInsert' (tpid + 1) t v dkm
        Just dkm' -> dkm'

-- FIXME: Will need to be in IO eventually?
getSrc :: AudioSource a -> FilePath
getSrc s = case s of
    SFile _ _ _ p -> p
    VFile _ _ _ _ p -> p

getRenderGraph :: GetPersistType -> AudioSource 'VirtualFile -> IO ExeGraph
getRenderGraph getPTy aSrc@(VFile _ d s g p) = do
    srcElems <- mapM (\src -> (sourceChannelCount src,) <$> persistFor getPTy src) s
    captureElem <- let outChans = getChannelCount aSrc in do
        PersistType pTy <- getPTy Capture outChans
        pure
            ( outChans
            , Element pTy [
                dkmInsert d (Nothing, idle) $
                dkmInsert zt (Nothing, active p zt) $
                Dkmap.empty]
            )
    pure $ ExeGraph $ execState (eGraph srcElems captureElem) g
  where
    addNodeS :: NodeName -> (a, b) -> State (Graph NodeName b d) (Maybe a)
    addNodeS n (c, e) = do
        g <- get
        case addNode n e g of
            Nothing -> pure Nothing
            Just g' -> do
                put g'
                pure $ Just c
    -- FIXME: The first let in this currently depends on persist having same
    -- number of ins and outs:
    asTidMap :: (Traversable t, Ord r) => ((Maybe NodeName, Word) -> r) -> t (Int, NodeName) -> Map r r
    asTidMap pc nids = fst $ foldl
        (\(acc, nPrev) (nChans, nid) ->
          ( foldl
              (\a' portId -> Map.insert
                  (pc (Nothing :: Maybe NodeName, portId))
                  (pc (Just nid, portId)) a')
                acc [0..fromIntegral nChans]
          , nPrev + nChans))
        (mempty, 0)
        nids
    replaceTerminals otm itm (opid, ipid) =
      ( maybe opid id $ Map.lookup opid otm
      , maybe ipid id $ Map.lookup ipid itm)
    addLike s e = do
        mn <- addNodeS (NodeName s) e
        case mn of
            Nothing -> addLike (s <> "_") e
            Just c -> pure $ (c, NodeName s)
    eGraph srcElems captureElem = do
        nids <- mapM (addLike "persist_src") srcElems
        let outTidMap = asTidMap OutPortId nids
        cNid <- addLike "persist_cap" captureElem
        let inTidMap = asTidMap InPortId [cNid]
        modify $ \gr -> gr
          { graphInputs = [], graphOutputs = []
          , graphConnections =
                Set.fromList $ replaceTerminals outTidMap inTidMap <$>
                Set.toList (graphConnections gr)
          }

liveSource :: SomeAudioSource -> Source
liveSource sas = withSomeAudioSource
    (\a -> dkmInsert
        zt
        ( Nothing
        , maybe (error "Unable to make a slice of live source") id $
            takeSlice sas zt $ getDuration a)
        Dkmap.empty)
    sas

dkmElems :: Dkmap k0 k1 v -> [v]
dkmElems = Map.elems . Dkmap.valueMap

requiredAudioSources :: Sources -> [SomeAudioSource]
requiredAudioSources =
    nub . fmap (sliceSource .  snd) . concatMap dkmElems . AL.values

runFor :: Time -> ExeGraph -> IO ()
runFor duration (ExeGraph g) =
    BB.withBulkBackend (SampleRate 44100) (PeriodSize 1024) $ \be -> do
        -- FIXME: Ignores all errors and events:
        merr <- runExceptT $ withRunningGraph be needsTypeAnnotation
        case merr of
            Left msg -> error msg
            Right u -> pure u
  where
    needsTypeAnnotation :: IO (Maybe String, [Either REError RageSessionEvent]) -> RunningGraph (Maybe String) x -> IO ()
    needsTypeAnnotation getEvt rg = do
        mapM (applyGraph rg) $ rageGraphControl newAudioGraph g
        applyTransp rg $ RtcTranspSetState TransportRolling
        e <- getEvt
        putStrLn $ "evt: " ++ show e
        -- Do something with duration instead of an arbitrary 10s sleep:
        threadDelay 10000000
        applyTransp rg $ RtcTranspSetState TransportStopped
        pure ()

mkLive :: GetPersistType -> SomeAudioSource -> IO Element
mkLive getPTy liveAudioSource = do
    mkPreReq liveAudioSource
    persistFor getPTy $ liveSource liveAudioSource
  where
    mkPreReq = withSomeAudioSource mkPreReq'
    mkPreReq' :: AudioSource a -> IO ()
    mkPreReq' audioSource = case audioSource of
        SFile {} -> pure ()
        VFile _ d s _ _ ->
          let
            preReqs = requiredAudioSources s
          in do
            rg <- getRenderGraph getPTy audioSource
            mapM mkPreReq preReqs >> runFor d rg
