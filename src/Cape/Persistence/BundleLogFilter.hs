{-# LANGUAGE
  GADTs
, PatternSynonyms
#-}
module Cape.Persistence.BundleLogFilter where

import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map

import qualified Clapi.Types.AssocList as AL
import Clapi.Types.Digests
  ( SomeFrDigest(..), withFrDigest, FrDigest(Frpd), DataDigest, Creates
  , OrderedContOps)
import Clapi.Types.Path
  ( Path, pattern Root, pattern (:/), isChildOfAny)
import Clapi.Types.Name (Placeholder, DataName)

import ClapiTranslator (engineN, transportS)

type LogEntry =
  (DataDigest, (Creates, OrderedContOps (Either Placeholder DataName)))

loggable :: SomeFrDigest -> Maybe LogEntry
loggable = withFrDigest go
  where
    go :: FrDigest a b -> Maybe LogEntry
    go frd = case frd of
        Frpd ns dd cr co -> if ns == engineN
          then loggableFrpd dd cr co
          else Nothing
        _ -> Nothing

loggableFrpd
  :: DataDigest -> Creates -> OrderedContOps (Either Placeholder DataName)
  -> Maybe LogEntry
loggableFrpd dd cr co =
  let
    excludedPath = not . flip isChildOfAny noLogTrees
    dd' = AL.filterKey excludedPath dd
    cr' = Map.filterWithKey (const . excludedPath) cr
    co' = Map.filterWithKey (const . excludedPath) co
  in if AL.null dd' && null cr' && null co'
    then Nothing
    else Just (dd', (cr', co'))

fromLogEntry :: LogEntry -> SomeFrDigest
fromLogEntry (dd, (cr, co)) = SomeFrDigest $ Frpd engineN dd cr co

-- FIXME: May not belong in here
noLogTrees :: Set Path
noLogTrees = Set.fromList
  [ Root :/ transportS
  ]
