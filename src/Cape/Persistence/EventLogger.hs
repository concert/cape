module Cape.Persistence.EventLogger where

import Blaze.ByteString.Builder (toByteString)
import Control.Monad (forever)
import Control.Monad.IO.Class (MonadIO(liftIO))
import Data.Attoparsec.ByteString (parse, IResult(..))
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC8
import Data.Maybe (fromJust)
import System.Directory (doesFileExist)

import Clapi.Serialisation.Base (Encodable(..), Decodable(..))
import Clapi.Protocol (Protocol, waitThen, sendFwd, sendRev)

logInbound
  :: (Encodable l, Decodable l) => (a -> Maybe l) -> (l -> a) -> FilePath
  -> Protocol a a b b IO ()
logInbound loggable fromLog fPath = do
    existingLog <- liftIO $ doesFileExist fPath
    if existingLog
      then loadExisting
      else liftIO $ writeVersionHeader
    forever $ waitThen fwd sendRev
  where
    logFormatId = BC8.pack "cape"
    logVersion = B.pack [0, 0, 0]
    logHeader = logFormatId <> logVersion
    loadExisting = do
        fContents <- liftIO $ B.readFile fPath
        case B.stripPrefix logHeader fContents of
            Just bundleBytes -> fwdAll $ parse parser bundleBytes
            Nothing -> error "Incompatible log format"
    fwdAll result = case result of
        Fail _ _ctxs err -> error err
        Partial _cont -> error "Truncated log"
        Done unconsumed a -> do
            sendFwd (fromLog a)
            if B.null unconsumed
              then pure ()
              else fwdAll (parse parser unconsumed)
    writeVersionHeader = B.writeFile fPath logHeader
    fwd a = do
        case loggable a of
            Nothing -> return ()
            Just a' -> liftIO $ B.appendFile fPath
                $ fromJust $ toByteString <$> builder a'
        sendFwd a
