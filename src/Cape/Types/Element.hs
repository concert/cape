module Cape.Types.Element where

import Clapi.Types (SomeWireValue)
import Clapi.Tree (TimeSeries)

import Rage.Loader (Type)

data Element = Element {
    elementType :: Type,
    timeSeries :: [TimeSeries [SomeWireValue]]} deriving (Eq)

-- FIXME: Not a "real" show (because show for Type is a tricky issue):
instance Show Element where
    show _ = "<Element>"
