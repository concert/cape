{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Cape.Types.Graph
  ( Graph(..), InPortId(..), OutPortId(..), Node(..), addNode
  , adjustNode, removeNode, addInput, addOutput, connect, disconnect
  , newGraph
  ) where

import Prelude hiding (fail)
import Control.Monad.Fail (MonadFail(..))
import Data.Word (Word)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import Text.Printf (printf)

newtype InPortId k = InPortId (Maybe k, Word) deriving (Eq, Ord)

newtype OutPortId k = OutPortId (Maybe k, Word) deriving (Eq, Ord)

data Node b d = Node {
    nodeBody :: b,
    nodeInputs :: [d],
    nodeOutputs :: [d]} deriving (Eq, Ord)

type Connection k = (OutPortId k, InPortId k)

data Graph k b d = Graph {
    graphOutputGetter :: b -> [d],
    graphInputGetter :: b -> [d],
    graphNodes :: Map k (Node b d),
    graphConnections :: Set (Connection k),
    graphInputs :: [d],
    graphOutputs :: [d]}

instance (Eq b, Eq d, Eq k) => Eq (Graph k b d) where
    (==) a b = all id comparables
      where
        comparables = [
            eqOf graphNodes, eqOf graphConnections,
            eqOf graphInputs, eqOf graphOutputs]
        eqOf t = (t a) == (t b)

instance (Show b, Show d) => Show (Graph k b d) where
    show g = printf "<Graph: nodes %d, connections %d>"
      (Map.size $ graphNodes g)
      (Set.size $ graphConnections g)

addNode :: (MonadFail m, Ord k) => k -> b -> Graph k b d -> m (Graph k b d)
addNode k b g = case Map.lookup k $ graphNodes g of
    Nothing -> pure $ g {graphNodes = Map.insert k
        (Node b (graphInputGetter g b) (graphOutputGetter g b))
        $ graphNodes g}
    Just _ -> fail "Node id clash"

adjustNode :: Ord k => (b -> b) -> k -> Graph k b d -> Graph k b d
adjustNode f i g = g {graphNodes = Map.adjust
    ((\nb -> Node nb (graphInputGetter g nb) (graphOutputGetter g nb)) . f . nodeBody) i
    $ graphNodes g}

removeNode :: Ord k => k -> Graph k b d -> Graph k b d
removeNode n g = g {graphNodes = ns', graphConnections = c}
  where
    ns' = Map.delete n $ graphNodes g
    c = Set.filter (not . refersTo) $ graphConnections g
    refersTo (OutPortId (onid, _), InPortId (inid, _)) = (onid == Just n) || (inid == Just n)

addInput :: d -> Graph k b d -> Graph k b d
addInput d g = g {graphInputs = graphInputs g ++ [d]}

-- FIXME: too similar to addInput
addOutput :: d -> Graph k b d -> Graph k b d
addOutput d g = g {graphOutputs = graphOutputs g ++ [d]}

-- FIXME: Doesn't guarantee acyclic!
connect :: Ord k => OutPortId k -> InPortId k -> Graph k b d -> Graph k b d
connect o i g = g {graphConnections = Set.insert (o, i) $ graphConnections g}

disconnect :: Ord k => OutPortId k -> InPortId k -> Graph k b d -> Graph k b d
disconnect o i g = g {graphConnections = Set.delete (o, i) $ graphConnections g}

newGraph :: Ord k => (b -> [d]) -> (b -> [d]) -> Graph k b d
newGraph og ig = Graph og ig mempty mempty mempty mempty
