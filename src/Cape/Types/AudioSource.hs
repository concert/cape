{-# LANGUAGE
    GADTs, DataKinds, KindSignatures, LambdaCase, RankNTypes
  , StandaloneDeriving
#-}

module Cape.Types.AudioSource
  ( AudioSourceType(..), SomeAudioSource(..), withSomeAudioSource, Slice
  , sliceSource, sliceStart, sliceEnd, Source, Sources, AudioSource(..)
  , getDuration, setDuration, getOrigin, setOrigin, getChannelCount, takeSlice
  , sourceChannelCount
  ) where

import qualified Data.Map as Map

import Clapi.Tree (Attributed)
import Clapi.Types.Dkmap (Dkmap)
import qualified Clapi.Types.Dkmap as Dkmap
import Clapi.Types.AssocList (AssocList)
import Clapi.Types.Base (Time, TpId)
import Clapi.Types.Name (DataName)

import Cape.Types.Graph (graphOutputs)
import Cape.Types.TimeOrigin (TimeOrigin)
import Cape.Types.Session (AudioGraph)

data AudioSourceType
  = VirtualFile
  | SourceFile
  deriving Eq

data SomeAudioSource = forall (a :: AudioSourceType). SomeAudioSource (AudioSource a)

instance Eq SomeAudioSource where
    (SomeAudioSource a) == (SomeAudioSource b) = case (a, b) of
        (VFile {}, VFile {}) -> a == b
        (SFile {}, SFile {}) -> a == b
        _ -> False

withSomeAudioSource :: (forall a. AudioSource a -> r) -> SomeAudioSource -> r
withSomeAudioSource f (SomeAudioSource src) = f src

data AudioSource (a :: AudioSourceType) where
  VFile :: TimeOrigin -> Time -> Sources -> AudioGraph -> FilePath -> AudioSource 'VirtualFile
  SFile :: TimeOrigin -> Time -> Int -> FilePath -> AudioSource 'SourceFile
deriving instance Eq (AudioSource a)

getDuration :: AudioSource a -> Time
getDuration = \case
    VFile _ d _ _ _ -> d
    SFile _ d _ _ -> d

-- If we want the slice invariants about not being off the end to hold then
-- this operation must somehow be tied to all slices of the source:
setDuration :: Time -> AudioSource 'VirtualFile -> AudioSource 'VirtualFile
setDuration t (VFile o _ s g p) = VFile o t s g p

getOrigin :: AudioSource a -> TimeOrigin
getOrigin = \case
    VFile o _ _ _ _ -> o
    SFile o _ _ _ -> o

setOrigin :: TimeOrigin -> AudioSource 'VirtualFile -> AudioSource 'VirtualFile
setOrigin o (VFile _ t s g p) = VFile o t s g p

getChannelCount :: AudioSource a -> Int
getChannelCount = \case
    VFile _ _ _ g _ -> length $ graphOutputs g
    SFile _ _ c _ -> c

data Slice = Slice
  { sliceSource :: SomeAudioSource
  , sliceStart :: Time
  , sliceEnd :: Time
  } deriving Eq

sliceChannelCount :: Slice -> Int
sliceChannelCount = withSomeAudioSource getChannelCount . sliceSource

type Source = Dkmap TpId Time (Attributed Slice)

sourceChannelCount :: Source -> Int
sourceChannelCount src = case Map.toList $ Dkmap.valueMap src of
    (_, (_, sl)) : _ -> sliceChannelCount sl
    [] -> 0

type Sources = AssocList DataName Source

takeSlice :: SomeAudioSource -> Time -> Time -> Maybe Slice
takeSlice src start end = let d = withSomeAudioSource getDuration src in
    if start < end && start < d && end <= d
        then Just $ Slice src start end
        else Nothing
