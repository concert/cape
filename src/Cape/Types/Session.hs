module Cape.Types.Session where
import Data.Map (Map)
import Data.Text (Text)

import Clapi.Types (Time(..), DataName, TpId, Attributee)
import Cape.Types.Element (Element, elementType)
import Cape.Types.Graph (Graph, newGraph)
import Rage.Loader (StreamDef(..), typeInstanceSpec, InstanceSpec(..))
import Rage.Graph (TransportState(TransportStopped))

data TransportInfo = TransportInfo
  { tiCue :: Time
  , tiState :: TransportState
  , tiChanged :: Time
  } deriving (Eq, Show)

newtype NodeName = NodeName Text deriving (Eq, Show, Ord)

type AudioGraph = Graph NodeName Element StreamDef

data Session = Session
  { apsGraph :: AudioGraph
  , apsTransp :: TransportInfo
  } deriving (Eq, Show)

transportInfoEmpty :: TransportInfo
transportInfoEmpty = TransportInfo zt TransportStopped zt

zt :: Time
zt = Time 0 0

inputGet, outputGet :: Element -> [StreamDef]
inputGet = isInputs . typeInstanceSpec . elementType
outputGet = isOutputs . typeInstanceSpec . elementType

newAudioGraph :: Graph NodeName Element StreamDef
newAudioGraph = newGraph outputGet inputGet

newSession :: Session
newSession = Session newAudioGraph transportInfoEmpty

-- Rest of the stuff in this file is for preserving attribution:

type TouchedPoints = Map DataName (Map TpId (Maybe Attributee))
newtype TouchedStuff = TouchedStuff
  { touchedTimePoints :: Map NodeName TouchedPoints
  }

instance Semigroup TouchedStuff where
    (<>) (TouchedStuff ttp) (TouchedStuff ttp') = TouchedStuff $ ttp <> ttp'

instance Monoid TouchedStuff where
    mempty = TouchedStuff mempty

type ModifiedSession = (TouchedStuff, Session)
