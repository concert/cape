module Server (serve) where
import Control.Monad (void, forever)
import Control.Monad.Trans (lift)
import Network.Socket.ByteString (recv, send)
import Network.Simple.TCP (connect, Socket, SockAddr)
import System.Clock (Clock(Monotonic))

import Cape.Types.Session (TouchedStuff, Session, newSession)
import RageGraphSync (REError, RageSessionEvent, RunningGraph, apply)
import ClapiEventApplier (LoaderResults, apiProto)
import RageEventTranslator (rageActions)
import Clapi.Types (TimeStamped(..))
import Clapi.Protocol (Protocol, waitThen, sendFwd, sendRev, (<<->), runProtocolIO)
import Clapi.SerialisationProtocol (serialiser)
import ClockSource (ClockSource(now))
import Cape.Persistence.EventLogger (logInbound)
import Cape.Persistence.BundleLogFilter (loggable, fromLogEntry)

import Clapi.Serialisation.Digests ()

timestampOutbound :: ClockSource c => c -> Protocol a a (TimeStamped b) b IO ()
timestampOutbound c = forever $ waitThen sendFwd rev
  where
    rev b = do
        t <- lift $ now c
        sendRev $ TimeStamped (t, b)

serve'
  :: (IO (TouchedStuff, [Either REError RageSessionEvent]), RunningGraph TouchedStuff p)
  -> LoaderResults -> Session -> (Socket, SockAddr) -> IO ()
serve' (nxtEvt, be) loaderResults startState (sock, _addr) = do
    startTime <- now clock
    -- FIXME: sending can be partial (and the void below just lobs away that info)
    runProtocolIO
        (recv sock 1024) (apply be) (void . send sock) nxtEvt
        (totalProto startTime)
  where
    clock = Monotonic
    totalProto startTime =
        serialiser <<-> logInbound loggable fromLogEntry "/tmp/cape_session" <<-> timestampOutbound clock
        <<-> apiProto startTime loaderResults startState
        -- <<-> stripUnconnectedProto
        <<-> rageActions newSession

serve
  :: (IO (TouchedStuff, [Either REError RageSessionEvent]), RunningGraph TouchedStuff p)
  -> LoaderResults -> Session -> IO ()
serve be loaderResults startState =
    connect "127.0.0.1" "1234" $ serve' be loaderResults startState
