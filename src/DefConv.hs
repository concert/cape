{-# LANGUAGE
    ApplicativeDo
  , OverloadedStrings
  , ScopedTypeVariables
  , TupleSections
  , QuasiQuotes
#-}

module DefConv
  (rtdsToCro, croToPd, croToStructDef, translateValues, ClapiReadyOpts, itdPair
  ) where

import Prelude hiding (fail)

import Control.Monad.Fail (MonadFail(..))
import Data.Maybe (fromJust)
import Data.Monoid ((<>))
import Data.Text (Text)
import qualified Data.Text as T

import Clapi.Util (strictZipWith, fmtStrictZipError)
import Clapi.Types
  ( TupMemberName, PostArgName, castName, InterpolationLimit
  , InterpolationType(..), mkName, SomeTreeType, bounds
  , PostDefinition(..), SomeWireValue, SomeDefinition(..)
  , Editability, DefName, Name
  , tupleDef, structDef, ttString, ttFloat, ttInt32, ttEnum, ttTime)
import Clapi.Types.AssocList (AssocList)
import qualified Clapi.Types.AssocList as AL
import qualified Rage.Loader as L
import Rage.Loader (InterpolableTupleDef, AtomDef(..), Constraints(..))
import Rage.Types.Atoms (Tuple)

fromAtomDef :: MonadFail m => L.AtomDef -> m SomeTreeType
fromAtomDef atomDef = case adConstraints atomDef of
    ConTime b -> if b == L.Bounds Nothing Nothing
        then pure ttTime
        else fail "There aren't bounded times in clapi"
    ConInt b -> ttInt32 <$> clBound fromIntegral b
    ConFloat b -> ttFloat <$> clBound id b
    ConString reStr -> pure $ ttString $ T.pack $ maybe "" id reStr
    ConEnum opts -> pure $ ttEnum $ map L.eoName opts
  where
    clBound c (L.Bounds l u) = bounds (c <$> l) (c <$> u)

fromFieldDef :: MonadFail m => L.FieldDef -> m (TupMemberName, SomeTreeType)
fromFieldDef (L.FieldDef name atomDef) = (,)
    <$> mkName (T.pack name) <*> fromAtomDef atomDef

data ClapiReady = ClapiReady
  { crDoc :: Text
  , crFields :: AssocList TupMemberName SomeTreeType
  , crDefaults :: Maybe Tuple
  , crInterpLimit :: InterpolationLimit
  } deriving (Eq, Show)

type ClapiReadyOpts = AssocList DefName ClapiReady

rtdToCr :: MonadFail m => InterpolationLimit -> L.TupleDef -> m (DefName, ClapiReady)
rtdToCr iMode (L.TupleDef name desc defaults fields) = do
    nameName <- mkName $ T.pack name
    treeDefs <- mapM fromFieldDef fields >>= AL.mkAssocList
    pure (nameName, ClapiReady (T.pack desc) treeDefs defaults iMode)

rtdsToCro :: MonadFail m => [(InterpolationLimit, L.TupleDef)] -> m ClapiReadyOpts
rtdsToCro rtds = mapM (uncurry rtdToCr) rtds >>= AL.mkAssocList

croToPd :: ClapiReadyOpts -> PostDefinition
croToPd cro = PostDefinition allDoc allFields
  where
    allDoc = foldMap crDoc cro
    allFields :: AssocList PostArgName [SomeTreeType]
    allFields = fromJust $ AL.mapKeys castName $ AL.values . crFields <$> cro

croToStructDef
  :: Editability -> ClapiReadyOpts -> DefName
  -> [(DefName, SomeDefinition)]
croToStructDef ed cro prefix =
    (prefix, sd) : tupleDefs
  where
    sd = structDef "Auto generated"
      $ fromJust $ AL.mapKeys castName
      $ AL.fmapWithKey (\k v -> (prefixed k, ed)) cro
    tupleDefs = AL.foldlWithKey appendTd [] cro
    appendTd
      :: [(DefName, SomeDefinition)] -> DefName -> ClapiReady
      -> [(DefName, SomeDefinition)]
    appendTd acc k tts = mkTd k tts : acc
    mkTd :: DefName -> ClapiReady -> (DefName, SomeDefinition)
    -- FIXME: Docs from our arse, threw them away making PostDefinition, maybe
    -- we should keep them around for that.
    mkTd k cr = (prefixed k, tupleDef (crDoc cr) (crFields cr) (crInterpLimit cr))
    prefixed :: Name a -> DefName
    prefixed s = prefix <> castName s

-- FIXME: Actually do some checking here!
translateValue :: MonadFail m => (DefName, ClapiReady) -> [SomeWireValue] -> m (DefName, [SomeWireValue])
translateValue (s, ClapiReady _ fields defaults _) wvs = pure (s, wvs)

-- FIXME: Unable to do anything with defaults because positional args.
translateValues
  :: MonadFail m => ClapiReadyOpts -> [[SomeWireValue]] -> m (AssocList DefName [SomeWireValue])
translateValues cro wvs =
    (fmtStrictZipError "types" "args" $ strictZipWith translateValue (AL.unAssocList cro) wvs)
    >>= sequence >>= AL.mkAssocList

asInterpLimit :: L.InterpolationLimit -> InterpolationLimit
asInterpLimit im = case im of
    L.InterpolationConst -> Just ItConstant
    L.InterpolationLinear -> Just ItLinear

itdPair :: InterpolableTupleDef -> (InterpolationLimit, L.TupleDef)
itdPair (lim, td) = (asInterpLimit $ lim, td)
