{-# LANGUAGE OverloadedStrings, TupleSections, GADTs, LambdaCase #-}
module RageGraphSync where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Word (Word32)
import qualified Data.Text as T
import qualified Control.Concurrent.Chan.Unagi as U
import Control.Concurrent.MVar
import Control.Monad.Except
import System.Clock (Clock(Monotonic))

import ClockSource (now)
import Clapi.Types
  ( Time(..), Interpolation(..), SomeWireValue, WireValue(..), WireType(..)
  , withWireValue)
import qualified Clapi.Types.Dkmap as Dkmap
import Clapi.Tree (TimeSeries)
import qualified Rage.Graph as G
import qualified Rage.Event as E
import qualified Rage.Loader as L
import Rage.Types.Backend (Backend)
import Rage.Types.Atoms (Tuple, Atom(..))
import qualified Rage.Connect as C
import qualified Rage.Bindings.Chronology as RBC
import Cape.Types.Session (NodeName)

-- Control events are stuff "the user" did:

data RageGraphControl
  = RgcElementAdd NodeName L.Type [TimeSeries [SomeWireValue]]
  | RgcElementUpdate NodeName Word32 (TimeSeries [SomeWireValue])
  | RgcElementRemove NodeName
  | RgcConnect (Maybe NodeName, Word) (Maybe NodeName, Word)
  | RgcDisconnect (Maybe NodeName, Word) (Maybe NodeName, Word)
  deriving Eq

data RageTransportControl
  = RtcTranspSeek Time
  | RtcTranspSetState G.TransportState
  deriving Eq

data RageSessionControl
  = RscT RageTransportControl
  | RscG RageGraphControl
  deriving Eq

-- "Event" events are stuff that happened:

type REError = (Maybe NodeName, String)

data RageTransportEvent
  = RteTranspSeeked Time
  | RteTranspSet G.TransportState Time
  deriving (Eq, Show)

data RageGraphEvent
  = RgeElementAdded NodeName L.Type [TimeSeries [SomeWireValue]]
  | RgeElementUpdated NodeName Word32 (TimeSeries [SomeWireValue]) E.EventId
  | RgeElementRemoved NodeName
  | RgeConnected (Maybe NodeName, Word) (Maybe NodeName, Word)
  | RgeDisconnected (Maybe NodeName, Word) (Maybe NodeName, Word)
  deriving Eq

instance Show RageGraphEvent where
    show e = case e of
        RgeElementAdded {} -> "Elem Added"
        RgeElementUpdated {} -> "Elem updated"
        RgeElementRemoved {} -> "Elem removed"
        RgeConnected {} -> "Elem connected"
        RgeDisconnected {} -> "Elem disconnected"

data RageSessionEvent
  = RseT RageTransportEvent
  | RseG RageGraphEvent
  deriving (Eq, Show)

data RunningGraph a p = RunningGraph {
    rbBackend :: G.Graph p,
    rbEvents :: U.InChan (a, [Either REError RageSessionEvent]),
    rbElements :: MVar (Map NodeName (G.Node p))}

-- Conversions between Clapi types and RageHs ones:

rTime :: Time -> RBC.Time
rTime (Time s f) = RBC.Time (fromIntegral s) (fromIntegral f)

rInterp :: Interpolation -> G.InterpolationMode
rInterp clI = case clI of
    IConstant -> G.InterpolationConst
    ILinear -> G.InterpolationLinear
    IBezier _ _ -> error "Does not exist in Rage yet"

rAtom :: WireValue a -> Atom
rAtom (WireValue wt a) = case wt of
    WtInt32 -> AInt $ fromIntegral a
    WtFloat -> AFloat a
    WtTime -> ATime $ rTime a
    WtString -> AString $ T.unpack a
    -- FIXME: This will only hold until we get words in rage:
    WtWord32 -> AEnum $ fromIntegral a
    _ -> error "Not implemented"

rTup :: [SomeWireValue] -> Tuple
rTup = fmap $ withWireValue rAtom

stripTpIds :: TimeSeries [SomeWireValue] -> G.TimeSeries
stripTpIds = fmap (\(_attrib, (interp, clTup)) -> G.TimePoint (rTup clTup) (rInterp interp)) . Map.fromList . Map.elems . Dkmap.flatten (\k v -> (rTime k, v))

-- Do the actions, and work out what events to emit depending on the success or
-- otherwise of the aforementioned actions:

applyTransp :: RunningGraph a p -> RageTransportControl -> IO (Either REError RageTransportEvent)
applyTransp (RunningGraph g _ nodes) = \case
    RtcTranspSeek t -> G.transportSeek (rTime t) g >>= either
        (pure . Left . (Nothing,))
        (const $ pure $ Right $ RteTranspSeeked t)
    RtcTranspSetState ts -> do
        G.setTransportState ts g
        tc <- now Monotonic  -- FIXME: Should be soundcard time
        pure $ Right $ RteTranspSet ts tc

applyGraph :: RunningGraph a p -> RageGraphControl -> IO (Either REError RageGraphEvent)
applyGraph (RunningGraph g _ nodes) = \case
    RgcElementAdd i t vs -> G.addNode t (stripTpIds <$> vs) g >>= either
        (pure . Left . (Just i,))
        (\n -> modifyMVar_ nodes (return . Map.insert i n) >> pure (pure $ RgeElementAdded i t vs))
    RgcElementUpdate i sidx ts -> do
        -- FIXME: This will be really slow when more than one time series
        -- changes, the finalisation should be delayed until the end of the
        -- bundle:
        nmap <- readMVar nodes
        case Map.lookup i nmap of
            Just n -> do
                mf <- G.updateNode n (fromIntegral sidx) (stripTpIds ts)
                pure $ case mf of
                    Left msg -> Left (Just i, msg)
                    Right evtId -> Right $ RgeElementUpdated i sidx ts evtId
            Nothing -> pure $ Left (Just i, "Cannot update non-existent element")
    RgcElementRemove i -> modifyMVar nodes $ \ns -> case Map.lookup i ns of
        Just n -> G.removeNode n >> pure (Map.delete i ns, Right $ RgeElementRemoved i)
        Nothing -> pure (ns, Left (Just i, "Cannot remove non-existent element"))
    RgcConnect op ip -> connLike C.connect RgeConnected op ip
    RgcDisconnect op ip -> connLike C.disconnect RgeDisconnected op  ip
  where
    connLike action successEvt (mOutNid, outP) (mInNid, inP) = do
        nmap <- readMVar nodes
        let asGn = flip Map.lookup nmap
        result <- runExceptT $ C.withTransaction g $ \ct -> do
            case (sequence $ asGn <$> mOutNid, sequence $ asGn <$> mInNid) of
                (Just mOutGn, Just mInGn) -> action mOutGn (fromIntegral outP) mInGn (fromIntegral inP) ct
                _ -> throwError "Unrecognised connection end(s)"
        pure $ case result of
            Left msg -> Left (Nothing, msg)
            Right () -> Right $ successEvt (mOutNid, outP) (mInNid, inP)

apply' :: RunningGraph a p -> RageSessionControl -> IO (Either REError RageSessionEvent)
apply' rg = \case
    RscT evt -> fmap RseT <$> applyTransp rg evt
    RscG evt -> fmap RseG <$> applyGraph rg evt

apply :: RunningGraph a p -> (a, [RageSessionControl]) -> IO ()
apply be (a, evts) = mapM (apply' be) evts >>= pure . (a,) >>= U.writeChan (rbEvents be)

withRunningGraph :: (Monoid a, Backend b) => b -> (IO (a, [Either REError RageSessionEvent]) -> RunningGraph a p -> IO r) -> ExceptT String IO r
withRunningGraph be act = do
    (ic, oc) <- liftIO $ U.newChan
    mf <- liftIO (G.withGraph be (handleEvent ic) $ act' ic oc)
    monadFailAsExcept mf
  where
    act' ic oc g = do
        nodes <- liftIO $ newMVar mempty
        act (U.readChan oc) (RunningGraph g ic nodes)
    monadFailAsExcept mf = case mf of
        Left msg -> throwError msg
        Right r -> pure r
    -- FIXME: This just makes generic errors out of every rage event:
    handleEvent ic evt = U.writeChan ic (mempty, [Left (Nothing, show evt)])
