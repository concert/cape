{-# LANGUAGE LambdaCase #-}
module RageEventTranslator (rageActions, rageGraphControl) where
import qualified Data.Set as Set

import Clapi.Protocol (Protocol, waitThen, sendFwd, sendRev)
import Cape.Types.Session
  ( Session(..), ModifiedSession, TransportInfo(..), AudioGraph
  , TouchedStuff)
import Cape.Types.Element (elementType, timeSeries, Element(..))
import Cape.Types.Graph
  ( Node(nodeBody), Graph(..), addNode, adjustNode, removeNode, OutPortId(..)
  , InPortId(..))
import qualified Cape.Types.Graph as Graph
import RageGraphSync
  ( REError, RageSessionEvent(..), RageTransportEvent(..), RageGraphEvent(..)
  , RageSessionControl(..), RageTransportControl(..), RageGraphControl(..))
import Util (mapDiffEvents, replaceIdx)

-- Diff old and new states to produce events about what happened:

rageTranspControl :: TransportInfo -> TransportInfo -> [RageTransportControl]
rageTranspControl ot nt =
  let
    sm = if tiState ot == tiState nt
      then []
      else [RtcTranspSetState $ tiState nt]
    cm = if tiCue ot == tiCue nt then [] else [RtcTranspSeek $ tiCue nt]
  in sm ++ cm

rageGraphControl :: AudioGraph -> AudioGraph -> [RageGraphControl]
rageGraphControl og ng = nodeEvts ++ disconEvts ++ conEvts
  where
    nodeEvts = mapDiffEvents
        (\nid _ -> pure $ RgcElementRemove nid)
        (\nid n -> let b = nodeBody n in
            pure $ RgcElementAdd nid (elementType b) (timeSeries b))
        (\nid n n' ->
          let
            ts = timeSeries $ nodeBody n
            ts' = timeSeries $ nodeBody n'
            seriesChangeEvt sidx tsa tsb = if tsa == tsb
                then []
                else [RgcElementUpdate nid sidx tsb]
          in concat $ zipWith3 seriesChangeEvt [0..] ts ts')
        (graphNodes og)
        (graphNodes ng)
    conEvts = consOnlyIn RgcConnect ng og
    disconEvts = consOnlyIn RgcDisconnect og ng
    consOnlyIn cmd allGraph excludeGraph =
        fmap (\(OutPortId (outnid, outp), InPortId (innid, inp)) -> cmd (outnid, outp) (innid, inp))
        $ Set.toList $ Set.difference (graphConnections allGraph) (graphConnections excludeGraph)

rageDiffEvents :: Session -> Session -> [RageSessionControl]
rageDiffEvents a a' = transp ++ graph
  where
    transp = RscT <$> rageTranspControl (apsTransp a) (apsTransp a')
    graph = RscG <$> rageGraphControl (apsGraph a) (apsGraph a')

-- Apply events to the target state:

patchTransp :: TransportInfo -> RageTransportEvent -> TransportInfo
patchTransp tr = \case
    RteTranspSet ts tc -> tr {tiState=ts, tiChanged=tc}
    RteTranspSeeked t -> tr {tiCue=t}

patchGraph :: AudioGraph -> RageGraphEvent -> AudioGraph
patchGraph g = \case
    RgeElementAdded n t vs -> case addNode n (Element t vs) g of
        Left e -> error $ "Duplicate node addition " ++ e
        Right g' -> g'
    RgeElementUpdated nid sidx ts _evtId -> adjustNode
        (\(Element t vs) -> Element t $ replaceIdx ts (fromIntegral sidx) vs)
        nid g
    RgeElementRemoved i -> removeNode i g
    RgeConnected opid ipid -> Graph.connect (OutPortId opid) (InPortId ipid) g
    RgeDisconnected opid ipid -> Graph.disconnect (OutPortId opid) (InPortId ipid) g

ragePatchEvent :: Session -> Either REError RageSessionEvent -> Session
ragePatchEvent s = either errErr $ \case
    RseT evt -> s{apsTransp = patchTransp (apsTransp s) evt}
    RseG evt -> s{apsGraph = patchGraph (apsGraph s) evt}
  where
    errErr (mNid, msg) = error $ (show mNid) ++ ": " ++ msg

-- Protocol that transforms state into events (and vice versa):

rageActions
  :: Monad m => Session
  -> Protocol
    ModifiedSession
    (TouchedStuff, [RageSessionControl])
    ModifiedSession
    (TouchedStuff, [Either REError RageSessionEvent])
    m ()
rageActions lps = waitThen fwd rev
  where
    fwd (touchedStuff, nps) =
        sendFwd (touchedStuff, rageDiffEvents lps nps) >> rageActions lps
    rev (touchedStuff, evts) = let nps = foldl ragePatchEvent lps evts in
        sendRev (touchedStuff, nps) >> rageActions nps
