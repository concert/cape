{-# LANGUAGE
    ApplicativeDo
  , DataKinds
  , FlexibleContexts
  , FlexibleInstances
  , GADTs
  , OverloadedStrings
  , PatternSynonyms
  , ScopedTypeVariables
  , TupleSections
  , TypeApplications
  , TypeFamilies
  , QuasiQuotes
#-}

module Router where

import Control.Monad (foldM)
import Control.Monad.Except (ExceptT, runExceptT, throwError, catchError)
import Control.Monad.Writer (Writer, runWriter, tell)
import Data.Bifunctor (first)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text

import Data.Map.Mol (Mol)
import qualified Data.Map.Mol as Mol

import Clapi.Types
  (DataName, Namespace, Placeholder, Name, NameRole(..)
  , Path, pattern Root, pattern (:/), splitHead
  , DataErrorIndex(..), Attributee
  , FrpDigest, FrDigest(Frpd), CreateOp, DataChange
  )
import Clapi.Types.AssocList (AssocList(..))
import Clapi.Types.SequenceOps (SequenceOp)


type Errors = Mol DataErrorIndex Text

-- | Only allow Namespaces to be used to index the router tree at the top level
type family SubRouterNameRole (i :: NameRole) where
  SubRouterNameRole 'ForNamespace = 'ForData
  SubRouterNameRole 'ForData = 'ForData

type GetChild nr a = Name nr -> Maybe (SomeRouter (SubRouterNameRole nr) a)
newtype ChildHandler nr a = ChildHandler { unChildHandler :: GetChild nr a }

type HandleCreate a
  = Placeholder -> Maybe Attributee -> CreateOp -> Either Errors a
newtype CreateHandler a = CreateHandler { unCreateHandler :: HandleCreate a }

type HandleCop a
  = Either Placeholder DataName -> Maybe Attributee
  -> SequenceOp (Either Placeholder DataName)
  -> Either Errors a
newtype CopHandler a = CopHandler { unCopHandler :: HandleCop a }

type HandleDc a = DataChange -> Either Errors a
newtype DcHandler a = DcHandler { unDcHandler :: HandleDc a }


data RouterType = RtContainer | RtLeaf

data Router (rt :: RouterType) (nr :: NameRole) a where
  ContainerRouter ::
    { rChildHandler :: GetChild nr a
    , rCreateHandler :: Maybe (Path -> CreateHandler a)
    , rCopHandler :: Maybe (Path -> CopHandler a)
    } -> Router 'RtContainer nr a
  LeafRouter ::
    { rDcHandler :: Path -> DcHandler a
    } -> Router 'RtLeaf nr a

containerRouter
  :: GetChild nr a
  -> Maybe (Path -> HandleCreate a)
  -> Maybe (Path -> HandleCop a)
  -> Router 'RtContainer nr a
containerRouter gc cr cop =
  ContainerRouter gc (fmap CreateHandler <$> cr) (fmap CopHandler <$> cop)

leafRouter :: (Path -> HandleDc a) -> Router 'RtLeaf nr a
leafRouter f = LeafRouter $ DcHandler <$>  f

data SomeRouter (nr :: NameRole) a where
  SomeRouter :: Router rt nr a -> SomeRouter nr a


structRouter :: GetChild nr a -> Router 'RtContainer nr a
structRouter gc = ContainerRouter gc Nothing Nothing

nullContainerRouter :: Router 'RtContainer nr a
nullContainerRouter = structRouter (const Nothing)

captureRouter
 :: (Name nr -> SomeRouter (SubRouterNameRole nr) a) -> Router 'RtContainer nr a
captureRouter f = structRouter $ \name -> Just $ f name

captureLeafRouter :: (Path -> Name nr -> HandleDc a) -> Router 'RtContainer nr a
captureLeafRouter f = captureRouter $
  \name -> SomeRouter $ leafRouter $ \p -> f p name

captureCreateRouter :: (Name nr -> HandleCreate a) -> Router 'RtContainer nr a
captureCreateRouter f = structRouter $
  \name -> Just $ SomeRouter $ nullContainerRouter ! (CreateHandler $ f name)

fromList
  :: [(Name nr, SomeRouter (SubRouterNameRole nr) a)]
  -> Router 'RtContainer nr a
fromList l = structRouter $ flip Map.lookup $ Map.fromList l

namespaceRouter
  :: GetChild 'ForNamespace a -> Router 'RtContainer 'ForNamespace a
namespaceRouter = structRouter

addCreateHandler
  :: Router 'RtContainer i a -> (Path -> CreateHandler a)
  -> Router 'RtContainer i a
addCreateHandler r hdlr = r { rCreateHandler = Just hdlr }

addCopHandler
  :: Router 'RtContainer i a -> (Path -> CopHandler a)
  -> Router 'RtContainer i a
addCopHandler r hdlr = r { rCopHandler = Just hdlr }

class AddHandler hdlr where
  (!) :: Router 'RtContainer i a -> hdlr a -> Router 'RtContainer i a
instance AddHandler CreateHandler where
  r ! hdlr = addCreateHandler r $ const hdlr
instance AddHandler CopHandler where
  r ! hdlr = addCopHandler r $ const hdlr

getChildRouter
  :: Router 'RtContainer nr a -> Name nr
  -> Either Text (SomeRouter (SubRouterNameRole nr) a)
getChildRouter r name = case r of
  ContainerRouter {rChildHandler = gc} ->
    maybe (Left "Missing child router") return $ gc name

resolveNs
  :: Router 'RtContainer 'ForNamespace a -> Namespace
  -> Either Errors (SomeRouter 'ForData a)
resolveNs r ns = te (NamespaceError ns) $ getChildRouter r ns

class SpecificRouter rt2 where
  specificRouter :: Router rt1 i a -> Either Text (Router rt2 i a)

instance SpecificRouter 'RtLeaf where
  specificRouter r = case r of
    LeafRouter {} -> return r
    _ -> Left "Unexpected container router"

instance SpecificRouter 'RtContainer where
  specificRouter r = case r of
    ContainerRouter {} -> return r
    _ -> Left "Unexpected leaf router"


resolveRouter
  :: SpecificRouter rt2
  => Router rt1 'ForData a -> Path -> Either Errors (Router rt2 'ForData a)
resolveRouter router path = go router path Root
  where
    go
      :: SpecificRouter rt2
      => Router rt1 'ForData a -> Path -> Path
      -> Either Errors (Router rt2 'ForData a)
    go r remP curP =
      let ei = PathError path in case splitHead remP of
        Nothing -> te ei $ specificRouter r
        Just (s, remP') -> do
          (SomeRouter r') <- te ei $
            first ((Text.pack (show curP) <> ": ") <>) $
            specificRouter r >>= flip getChildRouter s
          go r' remP' (curP :/ s)


class Resolve x where
  resolve :: Router rt 'ForData a -> Path -> Either Errors (x a)

instance SpecificRouter rt => Resolve (Router rt 'ForData) where
  resolve r p = resolveRouter r p

instance Resolve DcHandler where
  resolve r p =
    resolveRouter r p >>= return . ($ p) . rDcHandler

instance Resolve CreateHandler where
  resolve r p = resolveRouter r p >>=
    maybe (die (PathError p) "No create handler") (return . ($ p))
    . rCreateHandler

instance Resolve CopHandler where
  resolve r p = resolveRouter r p >>=
    maybe (die (PathError p) "No Cop handler") (return . ($ p))
    . rCopHandler


type HandleM a r = ExceptT Errors (Writer (Errors, a)) r

runHandleM :: Monoid a => HandleM a () -> (Errors, a)
runHandleM m = let (eit, (errs, as)) = runWriter $ runExceptT m in
  either (\e -> (errs <> e, as)) (const (errs, as)) eit

res :: Monoid a => a -> HandleM a ()
res a = tell (mempty, a)

err :: Monoid a => Errors -> HandleM a ()
err e = tell (e, mempty)

err_ :: Monoid a => DataErrorIndex -> Text -> HandleM a ()
err_ ei msg = err $ Mol.singleton ei msg

fatal :: Monoid a => DataErrorIndex -> Text -> HandleM a ()
fatal ei msg = throwError $ Mol.singleton ei msg

capture :: Monoid a => HandleM a () -> HandleM a ()
capture m = catchError m err

critical :: Monoid a => Either Errors r -> HandleM a r
critical = either throwError return

tolerate :: Monoid a => Either Errors a -> HandleM a ()
tolerate = either err res




-- | Intended to turn a FrpDigest into some domain-specific data structure
--   detailing what to do to fulfill the request. As such, we handle many errors
--   in parallel!
handleFrpDigest
  :: Monoid a
  => Router 'RtContainer 'ForNamespace a -> FrpDigest
  -> (Errors, a)
handleFrpDigest router (Frpd ns dd cr cops) = runHandleM $ do
    (SomeRouter nsRouter) <- critical $ resolveNs router ns
    capture $ mapFoldMapMWithKey (handlePathCreates nsRouter) cr
    capture $ mapFoldMapMWithKey (handlePathCops nsRouter) cops
    capture $ alFoldMapMWithKey (handlePathDcs nsRouter) dd
  where
    handlePathCreates nsRouter p m = do
      f <- unCreateHandler <$> critical (resolve nsRouter p)
      mapFoldMapMWithKey (\ph (att, crOp) -> tolerate $ f ph att crOp) m

    handlePathCops nsRouter p m = do
      f <- unCopHandler <$> critical (resolve nsRouter p)
      alFoldMapMWithKey (\s (att, seqOp) -> tolerate $ f s att seqOp) m

    handlePathDcs nsRouter p dc = do
      f <- unDcHandler <$> critical (resolve nsRouter p)
      tolerate $ f dc



mapFoldMWithKey
  :: forall k a b m. Monad m => (b -> k -> a -> m b) -> b -> Map k a -> m b
mapFoldMWithKey f b m = Map.foldrWithKey f' return m b
  where
    f' :: k -> a -> (b -> m b) -> b -> m b
    f' k a fm b' = f b' k a >>= fm

mapFoldMapMWithKey
  :: forall k a b m. (Monoid b, Monad m) => (k -> a -> m b) -> Map k a -> m b
mapFoldMapMWithKey f = mapFoldMWithKey f' mempty
  where
    f' :: b -> k -> a -> m b
    f' b k a = (b <>) <$> f k a

alFoldMWithKey :: Monad m => (b -> k -> a -> m b) -> b -> AssocList k a -> m b
alFoldMWithKey f b = foldM (\b' (k, a) -> f b' k a) b . unAssocList

alFoldMapMWithKey
  :: forall k a b m. (Monoid b, Monad m)
  => (k -> a -> m b) -> AssocList k a -> m b
alFoldMapMWithKey f = alFoldMWithKey f' mempty
  where
    f' :: b -> k -> a -> m b
    f' b k a = (b <>) <$> f k a

die :: DataErrorIndex -> Text -> Either Errors a
die ei msg = Left $ Mol.singleton ei msg

te :: DataErrorIndex -> Either Text a -> Either Errors a
te ei = first $ Mol.singleton ei
