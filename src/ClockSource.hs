module ClockSource where
import Clapi.Types (Time(..))
import System.Clock (Clock, getTime, toNanoSecs)

class ClockSource a where
    now :: a -> IO Time

instance ClockSource Clock where
    now clock = toTime <$> getTime clock
      where
        toTime ts = let
            ns = realToFrac $ toNanoSecs ts :: Double
            (s, f) = properFraction $ ns / 10^(9 :: Int)
            ss = round $ 2^(32 :: Int) * f
          in
            Time s ss
